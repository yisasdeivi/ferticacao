

function registrarFertilizante(){
//creamos los datos
document.location.href = document.location.href;
var nombre=document.getElementById("nombre").value;
var estado=document.getElementById("estado").value;
var clasificacion=document.getElementById("clasificacion").value;
var nitrogeno=document.getElementById("nitrogeno").value;
var fosforo=document.getElementById("fosforo").value;
var potasio=document.getElementById("potasio").value;
var calcio=document.getElementById("calcio").value;
var zinc=document.getElementById("zinc").value;
var manganesio=document.getElementById("manganesio").value;
var magnesio=document.getElementById("magnesio").value;
var medida=document.getElementById("medida").value;
var detalle=document.getElementById("detalle").value;

if (nombre!= ""){
  $.ajax({
  type: 'POST',
  url: 'controlador/registrarFertilizante.php',
  data:{'nombre':nombre,'estado':estado,'clasificacion':clasificacion,'nitrogeno':nitrogeno,'fosforo':fosforo,'potasio':potasio,'calcio':calcio,'zinc':zinc,'manganesio':manganesio,'medida':medida,'detalle':detalle,'magnesio':magnesio},
          success:
              function(respuesta) {
              alert(respuesta);

   }
  });
}
else{
  alert("para registrar una finca debe de ingresar un nombre para la misma ");
}
}

//---------------------------Listar Lotes---------------------------------------//
$(document).ready(function() {
  $('#ejemploLote').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarLotePorUsuarioTabla.php",
          "type": "POST"
    },
    "columns": [
      { "data": "finca" },
      { "data": "idLote" },
      { "data": "lote" },
      { "data": "medida" },
      { "data": "ubicacion" },
      { "data": "fecha" },
      { "data": "estadofenologico" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});



//---------------------------Listar semillas---------------------------------------//
$(document).ready(function() {
  $('#ejemploSemilla').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarSemillaPorUsuarioTabla.php",
          "type": "POST"
    },
    "columns": [
      { "data": "finca" },
      { "data": "idSemilla" },
      { "data": "nombre" },
      { "data": "tipo" },
      { "data": "EpocaCultivo" },
      { "data": "peso" },
      { "data": "unidadMedida" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});




//---------------------------Listar Otros Insumos---------------------------------------//
$(document).ready(function() {
  $('#ejemploOtro').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarOtroPorUsuarioTabla.php",
          "type": "POST"
    },
    "columns": [
      { "data": "finca" },
      { "data": "idOtro" },
      { "data": "nombre" },
      { "data": "unidadMedida" },
      { "data": "detalle" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});

//---------------------------Listar Herramientas---------------------------------------//
$(document).ready(function() {
   $('#ejemploHerramienta').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarHerramientaPorUsuarioTabla.php",
          "type": "POST"
    },
    "columns": [
      { "data": "finca" },
      { "data": "idHerramienta" },
      { "data": "nombre" },
      { "data": "uso" },
      { "data": "cantidad" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});






//---------------------------Listar Empleados---------------------------------------//
$(document).ready(function() {
  $('#ejemploEmpleado').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarEmpleadoPorUsuarioTabla.php",
          "type": "POST"
    },
    "columns": [
      { "data": "Finca" },
      { "data": "idEmpleado" },
      { "data": "Nombre" },
      { "data": "Cedula" },
      { "data": "Telefono" },
      { "data": "Rol" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});

//---------------------------Listar Usuarios---------------------------------------//
$(document).ready(function() {
  $('#ejemploUsuario').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarUsuarios.php",
          "type": "POST"
    },
    "columns": [
      { "data": "nombre" },
      { "data": "cedula" },
      { "data": "correo" },
      { "data": "ocupacion" },
      { "data": "telefono" },
      { "data": "ciudad" },
      { "data": "fincas" },
      { "data": "lotes" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});


$(document).ready(function() {
  $('#ejemplo').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarFincaPorUsuarioTabla.php",
          "type": "POST"
    },
    "columns": [
      { "data": "propietario" },
      { "data": "finca" },
      { "data": "Ciudad" },
      { "data": "departamento" },
      { "data": "lotes" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});


//---------------------------Listar Fertilizantes---------------------------------------//
$(document).ready(function() {
  $('#ejemploFertilizante').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarFertilizantesUsuario.php",
          "type": "POST"
    },
    "columns": [
      { "data": "nombre" },
      { "data": "estado" },
      { "data": "clasificacion" },
      { "data": "N" },
      { "data": "P" },
      { "data": "K" },
      { "data": "Ca" },
      { "data": "Zn" },
      { "data": "Mg" },
      { "data": "Mn" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});



//---------------------------Listar Analisis por usuarios---------------------------------------//
$(document).ready(function() {
  $('#ejemploAnalisisU').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarAnalisisSuelo.php",
          "type": "POST"
    },
    "columns": [
      { "data": "Finca" },
      { "data": "Lote" },
      { "data": "Fecha" },
      { "data": "N" },
      { "data": "P" },
      { "data": "K" },
      { "data": "Ca" },
      { "data": "Zn" },
      { "data": "Mg" },
      { "data": "Mn" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});

//---------------------------Listar todo los analisis---------------------------------------//
$(document).ready(function() {
  $('#ejemploAnalisisA').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarAnalisisSueloA.php",
          "type": "POST"
    },
    "columns": [
      { "data": "Finca" },
      { "data": "Lote" },
      { "data": "Fecha" },
      { "data": "N" },
      { "data": "P" },
      { "data": "K" },
      { "data": "Ca" },
      { "data": "Zn" },
      { "data": "Mg" },
      { "data": "Mn" },
      { "data": "Usuario" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});

//---------------------------Listar todos los Fertilizantes ADMINISTRADOR---------------------------------------//
$(document).ready(function() {
  $('#ejemploFertilizanteA').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarFertilizantes.php",
          "type": "POST"
    },
    "columns": [
      { "data": "nombre" },
      { "data": "estado" },
      { "data": "clasificacion" },
      { "data": "N" },
      { "data": "P" },
      { "data": "K" },
      { "data": "Ca" },
      { "data": "Zn" },
      { "data": "Mg" },
      { "data": "Mn" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});



//---------------------------Generar Resultados por Usuario---------------------------------------//
$(document).ready(function() {
  $('#ejemplogenerarResultadosU').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarAnalisisSinRU.php",
          "type": "POST"
    },
    "columns": [
      { "data": "Finca" },
      { "data": "Lote" },
      { "data": "Fecha" },
      { "data": "N" },
      { "data": "P" },
      { "data": "K" },
      { "data": "Ca" },
      { "data": "Zn" },
      { "data": "Mg" },
      { "data": "Mn" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});

//---------------------------Listar resultados por usuario---------------------------------------//
$(document).ready(function() {
  $('#ejemploResultadosU').DataTable( {
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "controlador/listarResultadosSuelo.php",
          "type": "POST"
    },
    "columns": [
      { "data": "Finca" },
      { "data": "Lote" },
      { "data": "idSuelo" },
      { "data": "fechaR" },
      { "data": "N" },
      { "data": "P" },
      { "data": "K" },
      { "data": "Ca" },
      { "data": "Zn" },
      { "data": "Mg" },
      { "data": "Mn" },
      { "data": "acciones" }

      ],
    "oLanguage": {
            "sProcessing":     "Procesando...",
        "sLengthMenu": 'Mostrar <select>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="15">15</option>'+
            '<option value="20">20</option>'+
            '<option value="25">25</option>'+
            '<option value="-1">All</option>'+
            '</select> registros',
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Filtrar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Por favor espere - cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
        }
  });
});







 function registrarUsuario(){
          //Creamos los datos a enviar con el formulario

var Nombre=document.getElementById("nombre").value;
var correo=document.getElementById("correo").value;
var cedula=document.getElementById("cedula").value;
var ocupacion=document.getElementById("ocupacion").value;
var telefono=document.getElementById("telefono").value;
var clave=document.getElementById("clave").value;
var departamento=document.getElementById("depart").value;
var ciudad=document.getElementById("ciudad").value;
if(Nombre != "" && correo != "" && cedula != "" && ocupacion !="" && telefono !="" && clave != ""){
  if(!(isNaN(Nombre))){
  alert("El nombre no debe contener valores numericos");
  }

  else if(isNaN(cedula)){
  alert("la cedula no debe contener letras");
  }

  else if(isNaN(telefono)){
  alert("el telefono no debe contener letras");
  }
  else{
  $.ajax({
  type: 'POST',
  url: 'controlador/registrarUsuario.php',
  data:{'Nombre':Nombre,'correo':correo,'cedula':cedula,'ocupacion':ocupacion,'telefono':telefono,'clave':clave,'departamento':departamento,'ciudad':ciudad},
          success:
              function(respuesta) {
              alert(respuesta);
   }
  });
}
}else{
  alert("para registrar un usuario todos los campos deben ser rellenados");
}


}





function registrarUsuarioAdmin(){
         //Creamos los datos a enviar con el formulario

var Nombre=document.getElementById("nombre").value;
var correo=document.getElementById("correo").value;
var cedula=document.getElementById("cedula").value;
var ocupacion=document.getElementById("ocupacion").value;
var telefono=document.getElementById("telefono").value;
var clave=document.getElementById("clave").value;
var departamento=document.getElementById("depart").value;
var ciudad=document.getElementById("ciudad").value;
if(Nombre != "" && correo != "" && cedula != "" && ocupacion !="" && telefono !="" && clave != ""){
 if(!(isNaN(Nombre))){
 alert("El nombre no debe contener valores numericos");
 }

 else if(isNaN(cedula)){
 alert("la cedula no debe contener letras");
 }

 else if(isNaN(telefono)){
 alert("el telefono no debe contener letras");
 }
 else{
 $.ajax({
 type: 'POST',
 url: 'controlador/registrarUsuario.php',
 data:{'Nombre':Nombre,'correo':correo,'cedula':cedula,'ocupacion':ocupacion,'telefono':telefono,'clave':clave,'departamento':departamento,'ciudad':ciudad},
         success:
             function(respuesta) {
             alert(respuesta);
  }
 });
}
}else{
 alert("para registrar un usuario todos los campos deben ser rellenados");
}


}




function editarFinca(){
//creamos los datos
document.location.href = document.location.href;
var Nombre=document.getElementById("nombreF").value;
var idFinca=document.getElementById("idFinca").value;
if (Nombre!= ""){
  $.ajax({
  type: 'POST',
  url: 'controlador/editarFinca.php',
  data:{'Nombre':Nombre},
          success:
              function(respuesta) {
              alert(respuesta);

   }
  });
}
else{
  alert("para editar una finca debe de ingresar un nombre ");
}
}

function registrarLote(){

//creamos los datos
//
var nombre=document.getElementById("nombre").value;
var medida=document.getElementById("medida").value;
var fecha=document.getElementById("fecha").value;
var finca=document.getElementById("finca").value;
var estadoF=document.getElementById("estadoF").value;

if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/registrarLote.php',
  data:{'nombre':nombre,'medida':medida,'fecha':fecha,'finca':finca,'estadoF':estadoF},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);
   }
  });
}
else{
  alert("para registrar un lote todos los campos deben ser diligenciados");
}
}






function registrarSemilla(){

//creamos los datos
document.location.href = document.location.href;
var finca=document.getElementById("finca").value;
var nombre=document.getElementById("nombre").value;
var tipo=document.getElementById("tipo").value;
var epocaC=document.getElementById("epocaC").value;
var unidadM=document.getElementById("unidadM").value;
var peso=document.getElementById("peso").value;

if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/registrarSemilla.php',
  data:{'finca':finca,'nombre':nombre,'tipo':tipo,'epocaC':epocaC,'unidadM':unidadM,'peso':peso},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);
   }
  });
}
else{
  alert("para registrar una semilla todos los campos deben ser diligenciados");
}
}



function registrarOtro(){

//creamos los datos
//document.location.href = document.location.href;
var finca=document.getElementById("finca").value;
var nombre=document.getElementById("nombre").value;
var unidadM=document.getElementById("unidadM").value;
var detalle=document.getElementById("detalle").value;


if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/registrarOtro.php',
  data:{'finca':finca,'nombre':nombre,'unidadM':unidadM,'detalle':detalle},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);
   }
  });
}
else{
  alert("para registrar otro insumo todos los campos deben ser diligenciados");
}
}



function registrarEmpleado(){

//creamos los datos

var finca=document.getElementById("finca").value;
var nombre=document.getElementById("nombre").value;
var cedula=document.getElementById("cedula").value;
var telefono=document.getElementById("telefono").value;
var rol=document.getElementById("rol").value;


if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/registrarEmpleado.php',
  data:{'finca':finca,'nombre':nombre,'cedula':cedula,'telefono':telefono,'rol':rol},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);

   }
  });
}
else{
  alert("para registrar un empleado todos los campos deben ser diligenciados");
}

}


function registrarHerramienta(){

//creamos los datos

var finca=document.getElementById("finca").value;
var nombre=document.getElementById("nombre").value;
var uso=document.getElementById("uso").value;
var cantidad=document.getElementById("cantidad").value;



if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/registrarHerramienta.php',
  data:{'finca':finca,'nombre':nombre,'uso':uso,'cantidad':cantidad},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);

   }
  });
}
else{
  alert("para registrar una herramienta todos los campos deben ser diligenciados");
}

}


function registrarFinca(){
//creamos los datos
document.location.href = document.location.href;
var Nombre=document.getElementById("nombre").value;
var departamento=document.getElementById("depart").value;
var ciudad=document.getElementById("ciudad").value;

if (Nombre!= ""){
  $.ajax({
  type: 'POST',
  url: 'controlador/registrarFinca.php',
  data:{'Nombre':Nombre,'departamento':departamento,'ciudad':ciudad},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);
   }
  });
}
else{
  alert("para registrar una finca debe de ingresar un nombre para la misma ");
}
}



function editarFincas(){
//creamos los datos

var nombre=document.getElementById("nombreFE").value;
var departamento=document.getElementById("departFE").value;
var ciudad=document.getElementById("ciudadFE").value;
var finca=document.getElementById("idF").value;


if (nombre!= ""){
  $.ajax({
  type: 'POST',
  url: 'controlador/editarFinca.php',
  data:{'nombre':nombre,'departamento':departamento,'ciudad':ciudad,'finca':finca},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);
   }
  });
}
else{
  alert("para registrar una finca debe de ingresar un nombre para la misma ");
}
}

function ListarFinca(id){
//creamos los datos
  $.ajax({
  type: 'POST',
  url: 'controlador/listarFincaPorUsuario.php',
  data:{'id':id},
          success:
              function(respuesta) {
              $('#finca').html(respuesta);

   }
  });
}


function ListarFincaE(id){
//creamos los datos
  $.ajax({
  type: 'POST',
  url: 'controlador/listarFincaPorUsuario.php',
  data:{'id':id},
          success:
              function(respuesta) {
              $('#fincaE').html(respuesta);

   }
  });
}


function ListarHerramientas(herramienta){
//creamos los datos

  $.ajax({
  type: 'POST',
  url: 'controlador/listarHerramientaPorUsuario.php',
  data:{'herramienta':herramienta},
          success:
              function(respuesta) {
              $('#herr').html(respuesta);


   }
  });
}


function ListarSemillas(semilla){
//creamos los datos

  $.ajax({
  type: 'POST',
  url: 'controlador/listarSemillaPorUsuario.php',
  data:{'semilla':semilla},
          success:
              function(respuesta) {
              $('#Semi').html(respuesta);


   }
  });
}


function ListarOtros(otro){
//creamos los datos

  $.ajax({
  type: 'POST',
  url: 'controlador/listarOtroPorUsuario.php',
  data:{'otro':otro},
          success:
              function(respuesta) {
              $('#Otro').html(respuesta);


   }
  });
}


function ListarLotes(lote){
//creamos los datos

  $.ajax({
  type: 'POST',
  url: 'controlador/listarLotePorUsuario1.php',
  data:{'lote':lote},
          success:
              function(respuesta) {
              $('#Lot').html(respuesta);


   }
  });
}

function ListarFincas(finca){
//creamos los datos

  $.ajax({
  type: 'POST',
  url: 'controlador/listarFincaPorUsuario1.php',
  data:{'finca':finca},
          success:
              function(respuesta) {
              $('#Finc').html(respuesta);


   }
  });
}


function ListarEmpleados(empleado){
//creamos los datos

  $.ajax({
  type: 'POST',
  url: 'controlador/listarEmpleadoPorUsuario.php',
  data:{'empleado':empleado},
          success:
              function(respuesta) {
              $('#Empl').html(respuesta);


   }
  });
}


function ListarFertilizantes(fertilizante){
//creamos los datos

  $.ajax({
  type: 'POST',
  url: 'controlador/listarFertilizantePorUsuario.php',
  data:{'fertilizante':fertilizante},
          success:
              function(respuesta) {
              $('#Fert').html(respuesta);


   }
  });
}


function editarHerramientas(herramienta){
//creamos los datos
var finca=document.getElementById("fincaE").value;
var nombre=document.getElementById("nombreE").value;
var uso=document.getElementById("usoE").value;
var cantidad=document.getElementById("cantidadE").value;
var idH=document.getElementById("idH").value;



if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/editarHerramienta.php',
  data:{'finca':finca,'idH':idH,'nombre':nombre,'uso':uso,'cantidad':cantidad},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);(location.reload());

   }

  });
}
else{
  alert("para registrar una herramienta todos los campos deben ser diligenciados");
}

}




function editarSemillas(){

//creamos los datos

var finca=document.getElementById("fincaE").value;
var nombre=document.getElementById("nombreSE").value;
var tipo=document.getElementById("tipoSE").value;
var epocaC=document.getElementById("epocaCSE").value;
var unidadM=document.getElementById("unidadMSE").value;
var peso=document.getElementById("pesoSE").value;
var idS=document.getElementById("idS").value;

if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/editarSemilla.php',
  data:{'finca':finca,'nombre':nombre,'tipo':tipo,'epocaC':epocaC,'unidadM':unidadM,'peso':peso,'idS':idS},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);
   }
  });
}
else{
  alert("para registrar una semilla todos los campos deben ser diligenciados");
}
}



function editarOtros(){

//creamos los datos
//document.location.href = document.location.href;
var finca=document.getElementById("fincaE").value;
var nombre=document.getElementById("nombreOE").value;
var unidadM=document.getElementById("unidadMOE").value;
var detalle=document.getElementById("detalleOE").value;
var idO = document.getElementById("idO").value;


if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/editarOtro.php',
  data:{'finca':finca,'nombre':nombre,'unidadM':unidadM,'detalle':detalle,'idO':idO},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);
   }
  });
}
else{
  alert("para registrar otro insumo todos los campos deben ser diligenciados");
}
}


function editarEmpleados(empleado){
//creamos los datos
var finca=document.getElementById("fincaE").value;
var nombre=document.getElementById("nombreEm").value;
var cedula=document.getElementById("cedulaEm").value;
var telefono=document.getElementById("telefonoEm").value;
var rol=document.getElementById("rolEm").value;
var idE=document.getElementById("idE").value;


if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/editarEmpleado.php',
  data:{'finca':finca,'idE':idE,'nombre':nombre,'cedula':cedula,'telefono':telefono,'rol':rol},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);

   }
  });
}
else{
  alert("para registrar un empleado todos los campos deben ser diligenciados");
}

}





function editarFertilizantes(){
//creamos los datos
document.location.href = document.location.href;
var nombre=document.getElementById("nombreE").value;
var estado=document.getElementById("estadoE").value;
var clasificacion=document.getElementById("clasificacionE").value;
var nitrogeno=document.getElementById("nitrogenoE").value;
var fosforo=document.getElementById("fosforoE").value;
var potasio=document.getElementById("potasioE").value;
var calcio=document.getElementById("calcioE").value;
var zinc=document.getElementById("zincE").value;
var manganesio=document.getElementById("manganesioE").value;
var magnesio=document.getElementById("magnesioE").value;
var medida=document.getElementById("medidaE").value;
var detalle=document.getElementById("detalleE").value;
var idF=document.getElementById("idF").value;

if (nombre!= ""){
  $.ajax({
  type: 'POST',
  url: 'controlador/editarFertilizante1.php',
  data:{'nombre':nombre,'estado':estado,'clasificacion':clasificacion,'nitrogeno':nitrogeno,'fosforo':fosforo,'potasio':potasio,'calcio':calcio,'zinc':zinc,'manganesio':manganesio,'medida':medida,'detalle':detalle,'magnesio':magnesio,'idF':idF},
          success:
              function(respuesta) {
              alert(respuesta);

   }
  });
}
else{
  alert("para registrar una fertilizante debe de ingresar un nombre para el mismo ");
}
}


function editarLotes(){

//creamos los datos
//
var nombre=document.getElementById("nombreLE").value;
var medida=document.getElementById("medidaLE").value;
var fecha=document.getElementById("fechaLE").value;
var finca=document.getElementById("fincaE").value;
var estadoF=document.getElementById("estadoFLE").value;
var idLote=document.getElementById("idL").value;

if (nombre!=""){
  $.ajax({
  type:'POST',
  url:'controlador/editarLote.php',
  data:{'nombre':nombre,'medida':medida,'fecha':fecha,'finca':finca,'estadoF':estadoF,'idLote':idLote},
          success:
              function(respuesta) {
              swal("Guardado!", respuesta, "success");(respuesta);
   }
  });
}
else{
  alert("para registrar un lote todos los campos deben ser diligenciados");
}
}






function listarlotes(){
  var id=document.getElementById("finca").value;
  $.ajax({
  type: 'POST',
  url: 'controlador/listarFincaLotePorUsuario.php',
  data:{'idFinca':id},
          success:
              function(respuesta) {
              $('#lote').html(respuesta);

   }
  });
}

function listarlotesxusuario(){
  $.ajax({
  type: 'POST',
  url: 'controlador/listarLotePorUsuario.php',
  data:{'idFinca':id},
          success:
              function(respuesta) {
              $('#lote').html(respuesta);

   }
  });
}


function registrarAnalisis(){
  var idFinca=document.getElementById("finca").value;
  var idLote=document.getElementById("lote").value;
  var N=document.getElementById("N").value;
  var P=document.getElementById("P").value;
  var K=document.getElementById("K").value;
  var Mg=document.getElementById("Mg").value;
  var Zn=document.getElementById("Zn").value;
  var Ca=document.getElementById("Ca").value;
  var Mn=document.getElementById("Mn").value;


  swal({
  title: "Desea Guardar y Generar el Reporte?",
  text: "Generar la grafica de este analisis de Suelo!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si,Guardar y Generar reporte!",
  cancelButtonText: "No, quiero solo guardar!",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){


  if (isConfirm) {
    $.ajax({
  type: 'POST',
  url: 'controlador/registrarAnalisisdeSuelo.php',
  data:{'idFinca':idFinca,'idLote':idLote,'N':N,'P':P,'K':K,'Mg':Mg,'Zn':Zn,'Mn':Mn,'Ca':Ca},
          success:
              function(respuesta) {
              $('#lote').html(respuesta);
              swal("Guardado!", respuesta, "success");(respuesta);

   }
  });
  $.ajax({
  type: 'POST',
  url: 'controlador/resultadoAnalisisGrafica.php',
          success:
              function(respuesta) {
               $('#resultado').html(respuesta);


   }
  });
  } else {
       $.ajax({
  type: 'POST',
  url: 'controlador/registrarAnalisisdeSuelo.php',
  data:{'idFinca':idFinca,'idLote':idLote,'N':N,'P':P,'K':K,'Mg':Mg,'Zn':Zn,'Mn':Mn,'Ca':Ca},
          success:
              function(respuesta) {
              $('#lote').html(respuesta);
              swal("Guardado!", respuesta, "success");(respuesta);

   }
  });
  }
});
}


function Listardepartamento(){
//creamos los datos
  $.ajax({
  type: 'POST',
  url: 'controlador/Listardepartamento.php',
  data:{},
          success:
              function(respuesta) {
              $('#depart').html(respuesta);
              
   }
  });
}

function Listarciudad(){
//creamos los datos
  var id=document.getElementById("depart").value;
  $.ajax({
  type: 'POST',
  url: 'controlador/Listarciudad.php',
  data:{'id':id},
          success:
              function(respuesta) {
              $('#ciudad').html(respuesta);
              
   }
  });
}

function analisisGrafica(id){
  $.ajax({
  type: 'POST',
  url: 'controlador/resultadoAnalisisGraficaPorId.php',
  data:{'idSuelo':id},
          success:
              function(respuesta) {
               $('#result').html(respuesta);
              
   }
  });
}
//new

function Listarfincaxciudad(id){
//creamos los datos
  $.ajax({
  type: 'POST',
  url: 'controlador/Listarfincaxciudad.php',
  data:{'id':id},
          success:
              function(respuesta) {
              $('#finca').html(respuesta);
   }
  });
}
//cambiar contraseña

 function cambiarcontrasena()
 {
  var con1=document.getElementById('con1').value;
  var con2=document.getElementById('con2').value;
  if(con1!=con2 || con1=="" || con2=="")
    swal(
  'Oops...',
  'las contraseñas tienen que ser iguales o no ser vacias!',
  'error'
);

   $.ajax({
  type: 'POST',
  url: 'controlador/editarcontrasena.php',
  data:{'contra':con1},
          success:
              function(respuesta) {
                swal("Guardado!", respuesta, "success");(respuesta);      
            
   }
  });
 }


 function pedircontrasena()
 {
  var usuario=document.getElementById('Usuario').value;
  if(usuario=="")
    swal(
  'Oops...',
  'El usuario o cedula no tiene que estar vacio!',
  'error'
);

   $.ajax({
  type: 'POST',
  url: 'controlador/pedircontrasena.php',
  data:{'usuario':usuario},
          success:
              function(respuesta) {
                swal("Guardado!", respuesta, "success");(respuesta);      
            
   }
  });
 }