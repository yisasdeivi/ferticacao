<?php
        require_once("otros/encabezado.php");
        session_start();
    if(isset($_SESSION["id"])){?>
       <?php
       require_once("otros/navUsuario.php");
        ?>

        <meta charset="UTF-8">
        <title>Empleados</title>
        <!--CSS-->
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.css">
        <!--Javascript-->

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.bootstrap.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/operaciones.js"></script>

        <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
        </script>


        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Administracion de Empleados<small>.</small></h1>
            </div>
        </div>
        <section class="full-reset text-center" style="padding: 40px 0;">

            <article class="tile" data-toggle="modal" data-target="#modalEmpleado" type="submit">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-account-box-o"></i></div>
                <div class="tile-name all-tittles">Registrar Empleado</div>
                <div class="tile-num full-reset">1</div>
            </article>




            <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <h1>Empleado</h1>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <table id="ejemploEmpleado" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Finca</th>
                        <th>idEmpleado</th>
                        <th>Nombre</th>
                        <th>Cedula</th>
                        <th>Telefono</th>
                        <th>Rol</th>
                        <th>acciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>Finca</th>
                      <th>idEmpleado</th>
                      <th>Nombre</th>
                      <th>Cedula</th>
                      <th>Telefono</th>
                      <th>Rol</th>
                      <th>acciones</th>

                    </tr>
                    </tfoot>
                </table>
            </div>
      </div>



        </section>

       <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                </div>
                <div class="modal-body">
                    Bienvenido a la plataforma de control de nutrientes sobre los cultivos de cacao.

Siendo un usuario registrado en el sistema podras acceder a diferentes opciones las cuales se encuentran ubicadas en la parte izquierda de la pantalla.

Cada una de estas opciones permite realizar la administracion y gestion necesarias para ejercer un control basico sobre sus cultivos

nuestra plataforma prioriza el control sobre los nutrientes en el suelo de un cultivo en especifico
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                </div>
            </div>
          </div>
        </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="modalcambiar">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">Cambiar Contraseña!</h4>
                </div>
                <div class="modal-body">
                <div class="form-group">
                <h2> Por favor, para mayor seguridad cambie de contraseña frecuentemente</h2>
                     <p>Contraseña nueva:</p>
                     <input type="password" class="form-control" id="con1" required="">
                      <span class="input-group-btn"></span>

                       <p>Repita la Contraseña :</p>
                     <input type="password" class="form-control" id="con2" required="">
                      <span class="input-group-btn"></span>
            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cambiarcontrasena()"><i class="zmdi zmdi-thumb-up" ></i> &nbsp; Guardar</button>
                </div>
            </div>
          </div>
        </div>

        <!-- modal Registrar Empleado-->
        <div class="modal fade" id="modalEmpleado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
       <div class="modal-content">
         <script>setTimeout ("ListarFinca('<?php echo $_SESSION['id']?>')", 5);</script>
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
           <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Registrar Empleado</h4>
         </div>
         <div class="modal-body">
           <form>

             <div class="form-group">
                      <p>Finca:</p>

                      <select class="form-control" id="finca">
                      </select>
             </div>


              <div class="form-group">
                        <p>Nombre:</p>
                        <input type="text" class="form-control" id="nombre">
                         <span class="input-group-btn"></span>
               </div>

               <div class="form-group">
                         <p>Cedula:</p>
                         <input type="text" class="form-control" id="cedula" >
                          <span class="input-group-btn"></span>
                </div>

                <div class="form-group">
                          <p>Telefono:</p>
                          <input type="text" class="form-control" id="telefono" >
                          <span class="input-group-btn"></span>
                 </div>


               <div class="form-group">
                        <p>Rol:</p>
                        <select class="form-control" id="rol">
                           <option>Mantenimiento</option>
                           <option>Administrativo</option>
                        </select>
               </div>


             </form>
              </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
           <a type="button" class="btn btn-primary" onclick="registrarEmpleado();" data-dismiss="modal">Guardar</a>
         </div>
          </div>
           </div>
            </div>

      <!--termina modal Registrar Empleado-->



      <!-- modal Editar Empleado-->


       <script>setTimeout ("ListarFincaE('<?php echo $_SESSION['id']?>')", 5);</script>

      <div class="modal fade" id="modalEditE" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
     <div class="modal-content">

       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Editar Empleados</h4>
       </div>
       <div class="modal-body">
         <form>
                        <div class="form-group">
                                 <p>Finca:</p>

                                 <select class="form-control" id="fincaE">
                                 </select>
                        </div>
                        <div id="Empl">
                        </div>
           </form>

            </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
         <a type="button" class="btn btn-primary" onclick="editarEmpleados();" data-dismiss="modal">Guardar</a>
       </div>
       </div>
        </div>
         </div>


    <!--termina modal editar Empleado-->

        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            UFPS
                        </p>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© Desarrollador:2016 Carlos Alfaro</div>
        </footer>
    </div>
</body>

</html>
<?php
}else{
	header("location: index.php");
}
?>
