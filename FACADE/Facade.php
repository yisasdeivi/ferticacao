<?php


/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */
include_once '../NEGOCIO/OpUsuario.php';
include_once '../NEGOCIO/OpAdministrador.php';

class Facade
{

	private $OpUsuario;
	private $OpAdministrador;
	static $_instance;

	function __construct() {
       $this->OpUsuario =new OpUsuario();
       $this->OpAdministrador=new OpAdministrador();
   }

   private function __clone(){ }

   public static function getInstance(){
      if (!(self::$_instance instanceof self)){
         self::$_instance=new self();
      }
      return self::$_instance;
   }



	 	function validarLogin($usuario,$password,$rol){
         return $this->OpUsuario->validarLogin($usuario,$password,$rol);
		}

		function listarUsuarios(){
         return $this->OpAdministrador->listarUsuarios();
		}


		function listarFincasTabla($idUsuario){
			   return $this->OpUsuario->listarFincasTabla($idUsuario);
		}

		function listarLotesTabla($idUsuario){

			   return $this->OpUsuario->listarLotesTabla($idUsuario);
		}

		function listarSemillasTabla($idUsuario){

			   return $this->OpUsuario->listarSemillasTabla($idUsuario);
		}

		function listarOtrosTabla($idUsuario){

			   return $this->OpUsuario->listarOtrosTabla($idUsuario);
		}



		function listarHerramientasTabla($idUsuario){

			   return $this->OpUsuario->listarHerramientasTabla($idUsuario);
		}

		function listarEmpleadosTabla($idUsuario){

			   return $this->OpUsuario->listarEmpleadosTabla($idUsuario);
		}

		function listarFertilizantesUsuarioTabla($idUsuario){

				 return $this->OpUsuario->listarFertilizantesUsuarioTabla($idUsuario);
		}

		function listarAnalisisSuelo($idusuario){

				 return $this->OpUsuario->listarAnalisisSuelo($idusuario);
		}

		function listarAnalisisSueloA(){
			  return $this->OpAdministrador->listarAnalisisSueloA();
		}

		function listarAnalisisSinRU($idusuario){

				 return $this->OpUsuario->listarAnalisisSinRU($idusuario);
		}




		function listarResultadosSuelo($idusuario){

				 return $this->OpUsuario->listarResultadosSuelo($idusuario);
		}

		function eliminarFinca($idFinca){

				return $this->OpUsuario->eliminarFinca($idFinca);
		}

		function eliminarUsuario($idUsuario){

				return $this->OpAdministrador->eliminarUsuario($idUsuario);
		}

		function eliminarLote($idLote){

				return $this->OpUsuario->eliminarLote($idLote);
		}

		function eliminarSemilla($idSemilla){

				return $this->OpUsuario->eliminarSemilla($idSemilla);
		}


		function eliminarOtro($idOtro){

				return $this->OpUsuario->eliminarOtro($idOtro);
		}



		function eliminarHerramienta($idHerramienta){

				return $this->OpUsuario->eliminarHerramienta($idHerramienta);
		}

		function eliminarEmpleado($idEmpleado){

				return $this->OpUsuario->eliminarEmpleado($idEmpleado);
		}

        function eliminarAnalisisdeSuelo($idSuelo){
        	   return $this->OpUsuario->eliminarAnalisisdeSuelo($idSuelo);
		}

		function eliminarResultadosSuelo($idSuelo){
			  return $this->OpUsuario->eliminarResultadosSuelo($idSuelo);
		}
        

	 	function validarLogin2($usuario,$password,$rol){
         return $this->OpUsuario->validarLogin($usuario,$password,$rol);
		}

		function listarFincas($idUsuario){
			   return $this->OpUsuario->listarFincas($idUsuario);
		}


		function listarHerramienta($herramienta){
			   return $this->OpUsuario->listarHerramienta($herramienta);
		}


		function listarSemilla($idSemilla){
			   return $this->OpUsuario->listarSemilla($idSemilla);
		}

		function listarOtro($idotro){
			   return $this->OpUsuario->listarOtro($idotro);
		}


		function listarEmpleado($idEmpleado ){
			   return $this->OpUsuario->listarEmpleado($idEmpleado );
		}

		function listarFertilizante($idF){
			   return $this->OpUsuario->listarFertilizante($idF);
		}

		function listarLote($idLote ){
			   return $this->OpUsuario->listarLote($idLote);
		}

		function listarFinca($idFinca){
			   return $this->OpUsuario->listarFinca($idFinca);
		}


    function listardepart()
    {
     return $this->OpUsuario->listardepartamento();

    }
    function listarciudad($id){
    	return $this->OpUsuario->listarciudad($id);
    }
    function listarciudadxtiempo($id){
    	return $this->OpUsuario->listarciudadxtiempo($id);
    }
 function editarcontrasena($contra,$id)
  {
    return $this->OpUsuario->editarcontrasena($contra,$id);
  }
  function pedircontrasena($usuario)
   {
    return $this->OpUsuario->pedircontrasena($usuario);
   }
  




	/**
	 *
	 * @param ciudad
	 * @param contrase�a
	 * @param ocupacion
	 * @param correo
	 * @param telefono
	 * @param Nombre
	 * @param cedula
	 */
	function registrarUsuario($ciudad, $contrasena, $ocupacion, $correo, $telefono, $Nombre, $cedula)
	{
		return $this->OpUsuario->registrarUsuario($ciudad, $contrasena, $ocupacion, $correo, $telefono, $Nombre, $cedula);
	}

	/**
	 *
	 * @param ciudad
	 * @param idUsuario
	 * @param nombre
	 */
	function registrarFinca($ciudad, $nombre, $departamento,$idusuario)
	{
		return $this->OpUsuario->registrarFinca($ciudad, $nombre, $departamento,$idusuario);
	}


	function editarFinca($idFinca,$nombre,$ciudad,$departamento)
	{
		return $this->OpUsuario->editarFinca($idFinca,$nombre,$ciudad,$departamento);
	}

	function editarLote($idFinca,$idLote,$nombre,$medida,$fecha,$estadoF)
	{
		return $this->OpUsuario->editarLote($idFinca,$idLote,$nombre,$medida,$fecha,$estadoF);
	}

	function editarFertilizante($nombre,$idFinca)
	{
		return $this->OpUsuario->editarFinca($nombre,$idFinca);
	}
	/**
	 *
	 * @param nombre
	 * @param idFinca
	 */
	function registrarLote($nombre, $medida, $fecha, $idFinca,$estadoF)
	{
		return $this->OpUsuario->registrarLote($nombre, $medida, $fecha, $idFinca,$estadoF);
	}

	function registrarSemilla($idFinca,$nombre,$tipo,$epocaC,$unidadM,$peso)
	{
		return $this->OpUsuario->registrarSemilla($idFinca,$nombre,$tipo,$epocaC,$unidadM,$peso);
	}

	function registrarOtro($idFinca,$nombre,$unidadM,$detalle)
	{
		return $this->OpUsuario->registrarOtro($idFinca,$nombre,$unidadM,$detalle);
	}



	function registrarEmpleado($idFinca, $nombre, $cedula, $telefono, $rol)
	{
		return $this->OpUsuario->registrarEmpleado($idFinca, $nombre, $cedula, $telefono, $rol);
	}

	function registrarHerramienta($idFinca,$nombre,$uso,$cantidad)
	{
		return $this->OpUsuario->registrarHerramienta($idFinca,$nombre,$uso,$cantidad);
	}

	function editarHerramienta($idFinca,$idH,$nombre,$uso,$cantidad)
	{
		return $this->OpUsuario->editarHerramienta($idFinca,$idH,$nombre,$uso,$cantidad);
	}

	function editarSemilla($idFinca,$idS,$nombre,$tipo,$epocaC,$unidadM,$peso)
	{
		return $this->OpUsuario->editarSemilla($idFinca,$idS,$nombre,$tipo,$epocaC,$unidadM,$peso);
	}

	function editarOtro($idFinca,$ido,$nombre,$detalle,$unidadM)
	{
		return $this->OpUsuario->editarOtro($idFinca,$ido,$nombre,$detalle,$unidadM);
	}



	function editarEmpleado($idFinca,$idE,$nombre,$cedula,$telefono,$rol)
	{
		return $this->OpUsuario->editarEmpleado($idFinca,$idE,$nombre,$cedula,$telefono,$rol);
	}


	function 	editarFertilizante1($nombre, $estado,$clasificacion, $nitrogeno,$fosforo,$potasio,$calcio,$zinc,$manganesio, $medida,$detalle,$magnesio,$idF)
	{
		return $this->OpUsuario->	editarFertilizante1($nombre, $estado,$clasificacion, $nitrogeno,$fosforo,$potasio,$calcio,$zinc,$manganesio, $medida,$detalle,$magnesio,$idF);
	}

	




	/**
	 *
	 * @param medida
	 * @param estado
	 * @param funcion
	 * @param nombre
	 * @param azufre
	 * @param nitrgeno
	 * @param fosforo
	 * @param potasio
	 * @param detalle
	 */
	function registrarFertilizante($nombre, $estado,$clasificacion, $nitrogeno,$fosforo,$potasio, $calcio,$zinc,$manganesio, $medida,$detalle,$idusuario,$magnesio)
	{
		return $this->OpUsuario->registrarFertilizante($nombre, $estado,$clasificacion, $nitrogeno,$fosforo,$potasio, $calcio,$zinc,$manganesio, $medida,$detalle,$idusuario,$magnesio);
  }


function eliminarFusuario($idFertilizante){

	return $this->OpUsuario->eliminarFusuario($idFertilizante);


}

	/**
	 *
	 * @param zinc
	 * @param manganesio
	 * @param calcio
	 * @param magnesio
	 * @param potasio
	 * @param fosforo
	 * @param nitrogeno
	 * @param idLote
	 */
	function ingresarAnalisisdeSuelo($zinc, $manganesio, $calcio, $magnesio, $potasio, $fosforo, $nitrogeno, $idLote)
	{
		return $this->OpUsuario->ingresarAnalisisdeSuelo($zinc, $manganesio,$calcio, $magnesio, $potasio, $fosforo, $nitrogeno, $idLote);
	}

    /**
    *Lista las fincas y los lotes y los monta en un combo recibe como parame id usuario
    *@param idUsuario
    *
    *
    */
   function listarFincaLotePorUsuario($idUsuario){
        return $this->OpUsuario->listarFincaLotePorUsuario($idUsuario);
   }
   /**
	 *
	 * @param zinc
	 * @param manganesio
	 * @param calcio
	 * @param magnesio
	 * @param potasio
	 * @param fosforo
	 * @param nitrogeno
	 * @param idLote
	 */

   function resultadoAnalisis($usuario){
   	    return $this->OpUsuario->resultadoAnalisis($usuario);
   }

   function resultadoAnalisisPorId($usuario,$idSuelo){
   	 return $this->OpUsuario->resultadoAnalisisPorId($usuario,$idSuelo);
   }


  }//llave final clasee
	?>
