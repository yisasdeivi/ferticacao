
      <?php
        require_once("otros/encabezado.php");
        session_start();
    if(isset($_SESSION["id"])){?>
     <?php
       require_once("otros/navUsuario.php");
        ?>
          <title>Plan de fertilizacion</title>
          <div class="prueba">
            <div class="fondo">
       <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Nuevo Analisis<small></small></h1>
            </div>
        </div>
         <script>setTimeout ("ListarFinca('<?php echo $_SESSION['id']?>')", 5);
         </script>
        <div class="container-fluid" id="resultado">
            <form autocomplete="off">
                <div class="container-flat-form">
                    <div class="title-flat-form title-flat-green">Nuevo Analisis de Suelo</div>
                    <div class="row">
                          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                            <legend><strong>Ingresar Informacion</strong></legend><br>
                                <!--Seleccion de Finca-->
                                <div class="form-group">
                                 <span>Seleccionar Finca</span>
                                  <div class="col-lg-10" id="select-finca">                                
                                  <select id="finca" class="form-control" data-live-search="true" onclick="listarlotes()">                                                                  
                                 </select><br>
                                </div>
                             </div>
                             <!--cierre Finca-->
                          <data-placement="bottom" title="Salir del sistema"></li>
                              <!--Seleccion de Lote-->
                             <div class="form-group">
                                 <span>Seleccionar Lote</span>
                                  <div class="col-lg-10">
                                  <script></script>
                                  <select id="lote" class="s form-control" data-live-search="true">
                                  
                                 </select>
                                </div>
                             </div>
                            

                            <!--cierre lote-->
            <div class="container-fluid" >
            <h2 class="text-center all-tittles">Ingresar Analisis</h2>
            <div class="table-responsive">
                <div class="div-table" style="margin:0 !important;">
                    <div class="div-table-row div-table-row-list" style="background-color:#DFF0D8; font-weight:bold;">
                        <div class="div-table-cell" style="width: 6%;" >#</div>
                        <div class="div-table-cell" style="width: 11%;">Nitrogeno(N)</div>
                        <div class="div-table-cell" style="width: 11%;">Fosforo(P)</div>
                        <div class="div-table-cell" style="width: 11%;">Potasio(K)</div>
                        <div class="div-table-cell" style="width: 11%;">Calcio(Ca)</div>                    
                        <div class="div-table-cell" style="width: 11%;">Magnesio(Mg)</div>
                        <div class="div-table-cell" style="width: 11%;">Manganeso(Mn)</div>
                        <div class="div-table-cell" style="width: 11%;">Zinc(Zn)</div>

                    </div>
                </div>
            </div>

            <script>
function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9-.]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
</script>
            <div class="table-responsive">
                <div class="div-table" style="margin:0 !important;">
                    <div class="div-table-row div-table-row-list">
                        <div class="div-table-cell" style="width: 6%;"></div>
                        <div class="div-table-cell" style="width: 10%;"><input class="form-control" id="N" onkeypress="return valida(event)"></div>
                        <div class="div-table-cell" style="width: 10%;"><input class="form-control" id="P"
                          onkeypress="return valida(event)" ></div>
                        <div class="div-table-cell" style="width: 10%;"><input class="form-control" id="K"
                        onkeypress="return valida(event)" ></div>
                         <div class="div-table-cell" style="width: 10%;"><input class="form-control" id="Ca"
                          onkeypress="return valida(event)" ></div>
                        <div class="div-table-cell" style="width: 10%;"><input class="form-control" id="Mg"
                         onkeypress="return valida(event)" ></div>
                        <div class="div-table-cell" style="width: 10%;"><input class="form-control" id="Mn"
                         onkeypress="return valida(event)" ></div>
                        <div class="div-table-cell" style="width: 10%;"><input class="form-control" id="Zn"
                        onkeypress="return valida(event)" ></div>

                    </div>
                </div>
            </div>
        </div>
                </div>
                </div>
                <p class="text-center">
                                <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                                <button type="button" class="btn btn-primary" onclick="registrarAnalisis()"><i class="zmdi zmdi-floppy"></i> &nbsp;&nbsp; Guardar</button>
                            </p>
            </form>
        </div>
       <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                </div>
                <div class="modal-body">
                    Bienvenido a la plataforma de control de nutrientes sobre los cultivos de cacao.

Siendo un usuario registrado en el sistema podras acceder a diferentes opciones las cuales se encuentran ubicadas en la parte izquierda de la pantalla.

Cada una de estas opciones permite realizar la administracion y gestion necesarias para ejercer un control basico sobre sus cultivos

nuestra plataforma prioriza el control sobre los nutrientes en el suelo de un cultivo en especifico
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                </div>
            </div>
          </div>
        </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="modalcambiar">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">Cambiar Contraseña!</h4>
                </div>
                <div class="modal-body">
                <div class="form-group">
                <h2> Por favor, para mayor seguridad cambie de contraseña frecuentemente</h2>
                     <p>Contraseña nueva:</p>
                     <input type="password" class="form-control" id="con1" required="">
                      <span class="input-group-btn"></span>

                       <p>Repita la Contraseña :</p>
                     <input type="password" class="form-control" id="con2" required="">
                      <span class="input-group-btn"></span>
            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cambiarcontrasena()"><i class="zmdi zmdi-thumb-up" ></i> &nbsp; Guardar</button>
                </div>
            </div>
          </div>
        </div>
        </form>
        </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            UFPS 2016                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Desarrollador</h4>
                        <ul class="list-unstyled">
                            <li><i class="zmdi zmdi-check zmdi-hc-fw"></i>&nbsp; Carlos Alfaro <i class="zmdi zmdi-facebook zmdi-hc-fw footer-social"></i><i class="zmdi zmdi-twitter zmdi-hc-fw footer-social"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© 2016 Carlos Alfaro</div>
        </footer>
    </div>
</body>
<script>
    function pruebaDivAPdf() {
      html2canvas(document.body, {
      onrendered: function(canvas) {
       var img=canvas.toDataURL('image/png');
       var pdf = new jsPDF();
       pdf.addImage(img,'JPEG',20,20);
       pdf.save('jesus.pdf');
  }
});
        

    }
</script>
<script src="js/operaciones.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
 <script src="js/html2canvas.js"></script>
</html>
<?php
}else{
  header("location: index.php");
}
?>