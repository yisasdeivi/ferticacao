  <?php
        require_once("otros/encabezado.php");

        session_start();
    if(isset($_SESSION["id"])){?>

      <?php
       require_once("otros/navUsuario.php");
        ?>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Sistema de Fertelizacion del Cacao<small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bienvenido <?php echo $_SESSION["usuario"] ?></small></h1>
            </div>
            <div class="text-center">
             <h3>Bienvenido a la Plataforma</h3>
             </div>
             <script>setTimeout ("Listarfincaxciudad('<?php echo $_SESSION['id']?>')", 5);</script>
       <div class="container-flat-form" style="width: 980px; margin-left:10px;">
        <section class="full-reset text-center" style="padding: 40px 0;">
        <select  id="finca" class="form-control" style="width:400px" onclick="tiempo()"> 
         </select>
          <div id='location'><h3 id='local'></h3></div>
          <div id="cont" style="">
          <div id="weather" class="weather">
            <div class="weather_date">DATE</div>
            <div class="weather_temp">
                <div class="weather_temp_min">MIN</div>
                <div class="weather_temp_max">MAX</div>
            </div>
            <img class="weather_icon">
            <div class="weather_text">
                
            </div>
        </div>
        </div>
  
        <div class="" style="width:200px; display:inline-block; text-align:center; margin-right:50px;">
            <caption><h3>Clima!</h3></caption>
            <table id="clima" class="table table-striped table-bordered" cellspacing="0" width="100%">
                
                <thead> 
                <th id="fecha"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                  <th id='clim'></th>                  
                </tr>
                <tr>
                  <th id="temp"></th>                  
                </tr>
                
                </tfoot>
            </table>
        </div> 


        <div class="" style="width:200px; display:inline-block; text-align:center;">
            <caption><h3>Atmosfera</h3></caption>
            <table id="tabla" class="table table-striped table-bordered" cellspacing="0" width="100%">
                
                <thead> 
                <th id="presion"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                  <th id='humedad'></th>                  
                </tr>
                <tr>
                  <th id="nubosidad"></th>                  
                </tr>
                <tr>
                  <th id="speed"></th>                  
                </tr>
                </tfoot>
            </table>
        </div> 
        </section>
   </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                </div>
                <div class="modal-body">
                    Bienvenido a la plataforma de control de nutrientes sobre los cultivos de cacao.

Siendo un usuario registrado en el sistema podras acceder a diferentes opciones las cuales se encuentran ubicadas en la parte izquierda de la pantalla.

Cada una de estas opciones permite realizar la administracion y gestion necesarias para ejercer un control basico sobre sus cultivos

nuestra plataforma prioriza el control sobre los nutrientes en el suelo de un cultivo en especifico
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                </div>
            </div>
          </div>
        </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="modalcambiar">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">Cambiar Contraseña!</h4>
                </div>
                <div class="modal-body">
                <div class="form-group">
                <h2> Por favor, para mayor seguridad cambie de contraseña frecuentemente</h2>
                     <p>Contraseña nueva:</p>
                     <input type="password" class="form-control" id="con1" required="">
                      <span class="input-group-btn"></span>

                       <p>Repita la Contraseña :</p>
                     <input type="password" class="form-control" id="con2" required="">
                      <span class="input-group-btn"></span>
            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cambiarcontrasena()"><i class="zmdi zmdi-thumb-up" ></i> &nbsp; Guardar</button>
                </div>
            </div>
          </div>
        </div>
     </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            UFPS
                        </p>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© Desarrollador:2016 Carlos Alfaro</div>
        </footer>
    </div>
</body>
<script src="js/operaciones.js"></script>
<script src="js/tiempo.js"></script>

</html>
<?php
}else{
    header("location: index.php");
}
?>
