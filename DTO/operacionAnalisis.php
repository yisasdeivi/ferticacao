<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:11:22 p.m.
 */

include_once ('../DTO/Suelo.php');
include_once ('../DTO/Ideal.php');
class operacionAnalisis
{

	var $Usuario;
	var $idFertilizante;
	var $nitrogeno;
	var $fosforo;
	var $potasio;
	var $magnesio;
	var $calcio;
	var $manganesio;
	var $zinc;
	var $idSuelo;
	

	function operacionAnalisis()
	{
		
	}
    
    
    function resultadoAnalisis(Suelo $suelo,Ideal $ideal,$Usuario){
      $this->Usuario=$Usuario;
      $this->nitrogeno=($ideal->getnitrogeno())-($suelo->getnitrogeno());
      $this->fosforo=($ideal->getfosforo())-($suelo->getfosforo());
      $this->potasio=($ideal->getpotasio())-($suelo->getpotasio());
      $this->magnesio=($ideal->getmagnesio())-($suelo->getmagnesio());
      $this->calcio=($ideal->getcalcio())-($suelo->getcalcio());
      $this->manganesio=($ideal->getmanganesio())-($suelo->getmanganesio());
      $this->zinc=($ideal->getzinc())-($suelo->getzinc());
      $N=round(100-((($suelo->getnitrogeno())/($ideal->getnitrogeno()))*100));
      $P=round(100-((($suelo->getfosforo())/($ideal->getfosforo()))*100));
      $K=round(100-((($suelo->getpotasio())/($ideal->getpotasio()))*100));
      $Mg=round(100-((($suelo->getmagnesio())/($ideal->getmagnesio()))*100));
      $Zn=round(100-((($suelo->getzinc())/($ideal->getzinc()))*100));
      $Ca=round(100-((($suelo->getcalcio())/($ideal->getcalcio()))*100));
      $Mn=round(100-((($suelo->getmanganesio())/($ideal->getmanganesio()))*100));
      $resultado=array('N' =>$N,'P' =>$P,'K' =>$K,'Mg' =>$Mg,'Zn' =>$Zn,'Ca' =>$Ca,'Mn'=>$Mn);
      return $resultado;
   }


	function getUsuario()
	{
		return $this->Usuario;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setUsuario($newVal)
	{
		$this->Usuario = $newVal;
	}

	function getidFertilizante()
	{
		return $this->idFertilizante;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setidFertilizante($newVal)
	{
		$this->idFertilizante = $newVal;
	}

	function getnitrogeno()
	{
		return $this->nitrogeno;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setnitrogeno($newVal)
	{
		$this->nitrogeno = $newVal;
	}

	function getfosforo()
	{
		return $this->fosforo;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setfosforo($newVal)
	{
		$this->fosforo = $newVal;
	}

	function getpotasio()
	{
		return $this->potasio;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setpotasio($newVal)
	{
		$this->potasio = $newVal;
	}

	function getmagnesio()
	{
		return $this->magnesio;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setmagnesio($newVal)
	{
		$this->magnesio = $newVal;
	}

	function getcalcio()
	{
		return $this->calcio;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setcalcio($newVal)
	{
		$this->calcio = $newVal;
	}

	function getmanganesio()
	{
		return $this->manganesio;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setmanganesio($newVal)
	{
		$this->manganesio = $newVal;
	}

	function getzinc()
	{
		return $this->zinc;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setzinc($newVal)
	{
		$this->zinc = $newVal;
	}



	function getidSuelo()
	{
		return $this->idSuelo;
	}

	/**
	 * 
	 * @param newVal
	 */
	function setidSuelo($newVal)
	{
		$this->idSuelo = $newVal;
	}


}
?>