<?php


/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:11:22 p.m.
 */
class Fertilizante
{
	var $idFertilizante;
	var $nombre;
	var $estado;
	var $clasificacion;
	var $nitrogeno;
	var $fosforo;
	var $potasio;
	var $calcio;
	var $manganesio;
	var $magnesio;
	var $zinc;
	var $medida;
	var $detalle;


	function Fertilizante()
	{
	}




	function getidFertilizante()
	{
		return $this->idFertilizante;
	}

	/**
	 *
	 * @param newVal
	 */
	function setidFertilizante($newVal)
	{
		$this->idFertilizante = $newVal;
	}

	function getnombre()
	{
		return $this->nombre;
	}

	/**
	 *
	 * @param newVal
	 */
	function setnombre($newVal)
	{
		$this->nombre = $newVal;
	}

	function getestado()
	{
		return $this->estado;
	}

	/**
	 *
	 * @param newVal
	 */
	function setestado($newVal)
	{
		$this->estado = $newVal;
	}



	function getnitrogeno()
	{
		return $this->nitrogeno;
	}

	/**
	 *
	 * @param newVal
	 */
	function setnitrogeno($newVal)
	{
		$this->nitrogeno = $newVal;
	}

	function getfosforo()
	{
		return $this->fosforo;
	}

	/**
	 *
	 * @param newVal
	 */
	function setfosforo($newVal)
	{
		$this->fosforo = $newVal;
	}

	function getpotasio()
	{
		return $this->potasio;
	}

	/**
	 *
	 * @param newVal
	 */
	function setpotasio($newVal)
	{
		$this->potasio = $newVal;
	}

	function getcalcio()
	{
		return $this->calcio;
	}

	/**
	 *
	 * @param newVal
	 */
	function setcalcio($newVal)
	{
		$this->calcio = $newVal;
	}



	function getmagnesio()
	{
		return $this->magnesio;
	}

	/**
	 *
	 * @param newVal
	 */
	function setmagnesio($newVal)
	{
		$this->magnesio = $newVal;
	}


	function getmanganesio()
	{
		return $this->calcio;
	}

	/**
	 *
	 * @param newVal
	 */
	function setmanganesio($newVal)
	{
		$this->manganesio = $newVal;
	}

	function getzinc()
	{
		return $this->zinc;
	}

	/**
	 *
	 * @param newVal
	 */
	function setzinc($newVal)
	{
		$this->zinc = $newVal;
	}



	function getmedida()
	{
		return $this->medida;
	}

	/**
	 *
	 * @param newVal
	 */
	function setmedida($newVal)
	{
		$this->medida = $newVal;
	}

	function getdetalle()
	{
		return $this->detalle;
	}

	/**
	 *
	 * @param newVal
	 */
	function setdetalle($newVal)
	{
		$this->detalle = $newVal;
	}

	function getclasificacion()
	{
		return $this->clasificacion;
	}

	/**
	 *
	 * @param newVal
	 */
	function setclasificacion($newVal)
	{
		$this->clasificacion = $newVal;
	}

}
?>
