<?php


/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:11:22 p.m.
 */
class Empleado
{

	var $idFinca;
	var $idEmpleado;
	var $nombre;
	var $cedula;
	var $telefono;
	var $rol;


	function Empleado()
	{
	}



	function getNombre()
	{
		return $this->nombre;
	}

	/**
	 *
	 * @param newVal
	 */
	function setNombre($newVal)
	{
		$this->nombre = $newVal;
	}

	function getCedula()
	{
		return $this->cedula;
	}

	/**
	 *
	 * @param newVal
	 */
	function setCedula($newVal)
	{
		$this->cedula = $newVal;
	}



	function getidFinca()
	{
		return $this->idFinca;
	}

	/**
	 *
	 * @param newVal
	 */
	function setidFinca($newVal)
	{
		$this->idFinca = $newVal;
	}

	function getidEmpleado()
	{
		return $this->idEmpleado;
	}

	/**
	 *
	 * @param newVal
	 */
	function setidEmpleado($newVal)
	{
		$this->idEmpleado = $newVal;
	}


	function getTelefono()
	{
		return $this->telefono;
	}

	/**
	 *
	 * @param newVal
	 */
	function setTelefono($newVal)
	{
		$this->telefono = $newVal;
	}


	function getRol()
	{
		return $this->rol;
	}

	/**
	 *
	 * @param newVal
	 */
	function setRol($newVal)
	{
		$this->rol = $newVal;
	}

}
?>
