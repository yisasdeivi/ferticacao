<?php


/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:11:22 p.m.
 */
class Semilla
{

	var $idFinca;
	var $idSemilla;
	var $nombre;
	var $tipo;
	var $epocaC;
	var $unidadM;
	var $peso;


	function Semilla()
	{
	}



	function gettipo()
	{
		return $this->tipo;
	}

	/**
	 *
	 * @param newVal
	 */
	function settipo($newVal)
	{
		$this->tipo = $newVal;
	}

	function getepocaC()
	{
		return $this->epocaC;
	}

	/**
	 *
	 * @param newVal
	 */
	function setepocaC($newVal)
	{
		$this->epocaC = $newVal;
	}



	function getidFinca()
	{
		return $this->idFinca;
	}

	/**
	 *
	 * @param newVal
	 */
	function setidFinca($newVal)
	{
		$this->idFinca = $newVal;
	}

	function getidSemilla()
	{
		return $this->idSemilla;
	}

	/**
	 *
	 * @param newVal
	 */
	function setidSemilla($newVal)
	{
		$this->idSemilla = $newVal;
	}


	function getnombre()
	{
		return $this->nombre;
	}

	/**
	 *
	 * @param newVal
	 */
	function setnombre($newVal)
	{
		$this->nombre = $newVal;
	}


	function getunidadM()
	{
		return $this->unidadM;
	}

	/**
	 *
	 * @param newVal
	 */
	function setunidadM($newVal)
	{
		$this->unidadM = $newVal;
	}

	function getpeso()
	{
		return $this->peso;
	}

	/**
	 *
	 * @param newVal
	 */
	function setpeso($newVal)
	{
		$this->peso = $newVal;
	}

}
?>
