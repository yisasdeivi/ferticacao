
<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once '../DTO/Usuario.php';
include_once '../DAO/Usuario_DAO.php';
include_once '../DTO/Administrador.php';
include_once '../DAO/Administrador_DAO.php';
include_once '../DTO/Finca.php';
include_once '../DAO/Finca_DAO.php';
include_once '../DTO/Lote.php';
include_once '../DAO/Lote_DAO.php';
include_once '../DTO/Empleado.php';
include_once '../DAO/Empleado_DAO.php';
include_once '../DTO/Herramienta.php';
include_once '../DAO/Herramienta_DAO.php';
include_once '../DTO/Semilla.php';
include_once '../DAO/Semilla_DAO.php';
include_once '../DTO/Otro.php';
include_once '../DAO/Otro_DAO.php';
include_once '../DTO/Fertilizante.php';
include_once '../DAO/Fertilizante_DAO.php';
include_once '../DTO/Fertilizante.php';
include_once '../DAO/Fertilizante_usuarioDAO.php';
include_once '../DTO/Suelo.php';
include_once '../DAO/Suelo_DAO.php';
include_once '../DTO/Resultado.php';
include_once '../DAO/Resultado_DAO.php';
include_once '../DTO/Ideal.php';
include_once '../DAO/Resultado_DAO.php';
include_once '../DAO/Ideal_DAO.php';
include_once '../DAO/Fertilizante_recomendadoDAO.php';
include_once '../DAO/departamento_DAO.php';
include_once '../DAO/ciudad_DAO.php';





class OpUsuario
{


	function __construct() {

    }


	/**
	 *
	 * @param ciudad
	 * @param contraseña
	 * @param ocupacion
	 * @param correo
	 * @param telefono
	 * @param Nombre
	 * @param cedula
	 */
	function registrarUsuario($ciudad, $contrasena, $ocupacion, $correo, $telefono, $Nombre, $cedula)
	{
		$Usuario= new Usuario();
		$UsuarioDAO= new Usuario_DAO();
		$Usuario->setNombre($Nombre);
		$Usuario->setciudad($ciudad);
		$Usuario->setcorreo($correo);
		$Usuario->setocupacion($ocupacion);
		$Usuario->settelefono($telefono);
		$Usuario->setcedula($cedula);
		$Usuario->setpassword($contrasena);
		$result=$UsuarioDAO-> AgregarUsuario($Usuario);

        if($result!=true){
            echo 'Error al registrar Cliente';
        }
        else {
            echo 'Registro Exitoso';
        }

	}




function listarUsuarios()
{
	$Usuario= new Usuario();
	$UsuarioDAO= new Usuario_DAO();

	$tabla = "";
	$result=$UsuarioDAO->listarUsuarios();
    while($row1=$UsuarioDAO->getArray($result))
		{
				$finca=new Finca();
				$fincaDAO=new Finca_DAO();
			  $finca->setidUsuario($row1['idusuario']);
				$fincas=$fincaDAO->cantidadFincas($finca);
				$cantFincas = $fincaDAO->getArray($fincas);
				$loteDAO = new Lote_DAO();
				$lotes=$loteDAO->cantidadLotesU($row1['idusuario']);
				$cantLotes = $loteDAO->getArray($lotes);
				$editar = '<a href='.$row1['idusuario'].'\" data-toggle=\"modal\" data-target=\"#modalEFinca\" type=\"submit\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Editar\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>';
				$eliminar = '<a href=\"controlador/eliminarLote.php?id='.$row1['idusuario'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este usuario?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';
				$tabla.='{
							"nombre":"'.$row1['nombre'].'",
							"cedula":"'.$row1['cedula'].'",
							"correo":"'.$row1['correo'].'",
							"ocupacion":"'.$row1['ocupacion'].'",
							"telefono":"'.$row1['telefono'].'",
							"ciudad":"'.$row1['ciudad'].'",
							"fincas":"'.$cantFincas['cantidadF'].'",
							"lotes":"'.$cantLotes['cantidadL'].'",
							"acciones":"'.$editar.$eliminar.'"
						},';
			}

						$tabla = substr($tabla,0, strlen($tabla) - 1);

						return $tabla;



}


	/**
	 *
	 * @param ciudad
	 * @param idUsuario
	 * @param nombre
	 */
	function registrarFinca($ciudad, $nombre, $departamento,$idusuario)
	{
		$finca = new Finca();
		$fincaDAO = new Finca_DAO();
		$finca->setnombre($nombre);
		$finca->setciudad($ciudad);
		$finca->setdepartamento($departamento);
		$finca->setidUsuario($idusuario);
		$result=$fincaDAO-> AgregarFinca($finca);

		if($result!=true){
				echo 'Error al registrar Finca';
		}
		else {
				echo 'Registro Exitoso';
		}
	}



	function eliminarFusuario($idFertilizante){
		$fertilizante = new Fertilizante();
		$fertilizanteDAO = new Fertilizante_DAO();
		$fertilizante_usuarioDAO= new Fertilizante_usuarioDAO();
		$fertilizante->setidFertilizante($idFertilizante);
		$result = $fertilizante_usuarioDAO->eliminarFusuario($fertilizante);
		$result = $fertilizanteDAO->eliminarFertilizantes($fertilizante);

		echo "fertilizante eliminada";

	}
	/**
	 *
	 * @param potasio
	 * @param fosforo
	 * @param nitrogeno
	 * @param estado
	 * @param nombre
	 * @param funcion
	 * @param azufre
	 * @param zinc
	 */

	function registrarFertilizante($nombre, $estado,$clasificacion, $nitrogeno,$fosforo,$potasio, $calcio,$zinc,$manganesio, $medida,$detalle,$idusuario,$magnesio)
	{
		$fertilizante = new Fertilizante();
		$fertilizanteDAO = new Fertilizante_DAO();
		$fertilizante_usuarioDAO = new Fertilizante_usuarioDAO();
		$fertilizante->setnombre($nombre);
		$fertilizante->setestado($estado);
		$fertilizante->setclasificacion($clasificacion);
		$fertilizante->setnitrogeno($nitrogeno);
		$fertilizante->setfosforo($fosforo);
		$fertilizante->setpotasio($potasio);
		$fertilizante->setcalcio($calcio);
		$fertilizante->setzinc($zinc);
		$fertilizante->setmagnesio($magnesio);
		$fertilizante->setmanganesio($manganesio);
		$fertilizante->setmedida($medida);
		$fertilizante->setdetalle($detalle);
		$result=$fertilizanteDAO-> AgregarFertilizante($fertilizante);
		$idFerti = $fertilizanteDAO->obtenerUltimo();
		$idFert=$fertilizanteDAO->getArray($idFerti);
		$result2=$fertilizante_usuarioDAO->AgregarFertilizanteU($idusuario,$idFert['id']);
		if($result!=true){
				echo 'Error al registrar Finca';
		}
		else {
				echo 'Registro Exitoso';
		}
	}

	function editarFinca($idFinca,$nombre,$ciudad,$departamento)
	{
		$finca = new Finca();
		$fincaDAO = new Finca_DAO();
		$finca->setNombre($nombre);
		$finca->setidFinca($idFinca);
		$finca->setCiudad($ciudad);
		$finca->setDepartamento($departamento);

		$result=$fincaDAO-> EditarFinca($finca);

		if($result!=true){
				echo 'Error al editar Finca';
		}
		else {
				echo 'Finca actualizada';
		}
	}


	function editarLote($idFinca,$idLote,$nombre,$medida,$fecha,$estadoF)
	{
		$lote = new Lote();
		$loteDAO = new Lote_DAO();
		$lote->setNombre($nombre);
		$lote->setestadoF($estadoF);
		$lote->setidLote($idLote);
		$lote->setMedida($medida);
		$lote->setFecha($fecha);
		$lote->setFinca($idFinca);
		$result=$loteDAO-> EditarLote($lote);

		if($result!=true){
				echo 'Error al editar Finca';
		}
		else {
				echo 'Finca actualizada';
		}
	}


	function editarFertilizante($nombre,$idFinca)
	{
		$finca = new Finca();
		$fincaDAO = new Finca_DAO();
		$finca->setNombre($nombre);
		$finca->setidFinca($idFinca);
		$result=$fincaDAO-> EditarFinca($finca);

		if($result!=true){
				echo 'Error al editar Finca';
		}
		else {
				echo 'Finca actualizada';
		}
	}

	function validarLogin($user,$password,$rol)
	{
		if ($rol==1) {
				$usuario=new Usuario();
				$UsuarioDAO=new Usuario_DAO();
				$usuario->setcedula($user);
				$usuario->setpassword($password);
				$resultado=$UsuarioDAO->Login($usuario);
				return $resultado;

		}

		 if ($rol==2) {
				$administrador = new Administrador();
				$AdministradorDAO=new Administrador_DAO();
				$administrador->setcedula($user);
				$administrador->setpassword($password);
				$resultado=$AdministradorDAO->Login($administrador);
				if($row=$AdministradorDAO->getArray($resultado)){
						return $row;
				}
				else {
						return false;
				}

		}
	}


//consulta   select count(idLote) from lote group by (idFinca)
		function ListarFincasTabla($idUsuario)
		{
          $usuario = new Usuario();
					$usuarioDAO = new Usuario_DAO();
					$usuario->setidUsuario($idUsuario);
					$nombres = $usuarioDAO->infoUsuario($usuario);
					$nombre = $usuarioDAO->getArray($nombres);
					$finca=new Finca();
					$fincaDAO=new Finca_DAO();
					$finca->setidUsuario($idUsuario);
					$resultado=$fincaDAO->ListarFincas($finca);
					$loteDAO = new Lote_DAO();

					$tabla = "";

					while($row=$fincaDAO->getArray($resultado))
					{
  					$lotes = $loteDAO->lotesxFinca($row['idFinca']);
						$lote = $loteDAO->getArray($lotes);

						$editar = '<a  data-id=\"'.$row['idFinca'].'\"\"type=\"button\" onclick=\"ListarFincas('.$row['idFinca'].')\" data-toggle=\"modal\" data-target=\"#modalEFinca\" data-placement=\"top\" title=\"Editar\" class=\"editar btn btn-primary\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>';

						$eliminar = '<a href=\"controlador/eliminarFinca.php?id='.$row['idFinca'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar esta finca?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';

						$tabla.='{
								  "propietario":"'.$nombre['nombre'].'",
								  "finca":"'.$row['nombre'].'",
								  "Ciudad":"'.$row['Ciudad'].'",
								  "departamento":"'.$row['departamento'].'",
									"lotes":"'.$lote['lotes'].'",
								  "acciones":"'.$editar.$eliminar.'"
								},';
      		}

								$tabla = substr($tabla,0, strlen($tabla) - 1);

								return $tabla;



		}


		function eliminarFinca($idFinca){
			$finca = new Finca();
			$fincaDao = new Finca_DAO();
			$finca->setidFinca($idFinca);
			$result = $fincaDao->eliminarFinca($finca);
			echo "finca eliminada";
		}

        function eliminarAnalisisdeSuelo($idSuelo){
        	$suelo= new Suelo();
        	$sueloDAO= new Suelo_DAO();
        	$suelo->setidSuelo($idSuelo);
        	$result=$sueloDAO->eliminarAnalisisSuelo($idSuelo);
        	echo $result;
        }

        function eliminarResultadosSuelo($idSuelo){
        	$resultado= new Resultado();
        	$resultadoDAO=new Resultado_DAO();
        	$resultado->setidResultado($idSuelo);
        	$result=$resultadoDAO->EliminarResultado($resultado);
        	echo "eliminador";
        }



		function listarFertilizantesUsuarioTabla($idUsuario)
		{
					$fertilizanteUDAO = new Fertilizante_usuarioDAO();
					$f=new Fertilizante_DAO();
					$fer = new Fertilizante();
					$tabla = "";
					$fertilizantes = $fertilizanteUDAO->listarFxU($idUsuario);

					while($fertilizante = $fertilizanteUDAO->getArray($fertilizantes))
					{
						$fer->setidFertilizante($fertilizante['idFertilizante']);
						$resultado = $f->ListarFertilizantes($fer);
						while($row=$f->getArray($resultado)){

							$editar = '<a  data-id=\"'.$row['idFertilizante'].'\"\"type=\"button\" onclick=\"ListarFertilizantes('.$row['idFertilizante'].')\" data-toggle=\"modal\" data-target=\"#modalEFert\" data-placement=\"top\" title=\"Editar\" class=\"editar btn btn-primary\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>';

							$eliminar = '<a href=\"controlador/eliminarFusuario.php?id='.$row['idFertilizante'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este fertilizante?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';

							$tabla.='{
										"nombre":"'.$row['nombre'].'",
										"estado":"'.$row['estado'].'",
										"clasificacion":"'.$row['clasificacion'].'",
										"N":"'.$row['nitrogeno'].'",
										"P":"'.$row['fosforo'].'",
										"K":"'.$row['potasio'].'",
										"Ca":"'.$row['calcio'].'",
										"Zn":"'.$row['zinc'].'",
										"Mg":"'.$row['magnesio'].'",
										"Mn":"'.$row['manganesio'].'",
										"acciones":"'.$editar.$eliminar.'"
									},';



             }

					}


								$tabla = substr($tabla,0, strlen($tabla) - 1);

								return $tabla;



		}







	function listarAnalisisSuelo($idusuario)
		{
					$sueloDAO = new Suelo_DAO();
					$listadoS = $sueloDAO->listarAnalisisSuelo($idusuario);
					$tabla = "";



					while($row = $sueloDAO->getArray($listadoS))
					{

							$eliminar = '<a href=\"controlador/eliminarAnalisisSuelo.php?id='.$row['idSuelo'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este analisis de suelo?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';
							$tabla.='{
										"Finca":"'.$row['finca'].'",
										"Lote":"'.$row['lote'].'",
										"Fecha":"'.$row['fecha'].'",
										"N":"'.$row['nitrogeno'].'",
										"P":"'.$row['fosforo'].'",
										"K":"'.$row['potasio'].'",
										"Ca":"'.$row['calcio'].'",
										"Zn":"'.$row['zinc'].'",
										"Mg":"'.$row['magnesio'].'",
										"Mn":"'.$row['manganesio'].'",
										"acciones":"'.$eliminar.'"
									},';



             }



								$tabla = substr($tabla,0, strlen($tabla) - 1);

								return $tabla;



		}


		function listarAnalisisSinRU($idusuario)
			{
						$sueloDAO = new Suelo_DAO();
						$listadoS = $sueloDAO->listarAnalisisSinRU($idusuario);
						$tabla = "";



						while($row = $sueloDAO->getArray($listadoS))
						{

								$eliminar = "<a id='".$row['idSuelo']."'onclick='analisisGrafica(".$row['idSuelo'].");' data-toggle='tooltip' data-placement='top' title='Generar' class='btn btn-success'><i class='fa fa-tree' aria-hidden='true'></i></a>";
								$tabla.='{
											"Finca":"'.$row['finca'].'",
											"Lote":"'.$row['lote'].'",
											"Fecha":"'.$row['fecha'].'",
											"N":"'.$row['nitrogeno'].'",
											"P":"'.$row['fosforo'].'",
											"K":"'.$row['potasio'].'",
											"Ca":"'.$row['calcio'].'",
											"Zn":"'.$row['zinc'].'",
											"Mg":"'.$row['magnesio'].'",
											"Mn":"'.$row['manganesio'].'",
											"acciones":"'.$eliminar.'"
										},';



							 }



									$tabla = substr($tabla,0, strlen($tabla) - 1);

									return $tabla;



			}








		function listarResultadosSuelo($idusuario)
			{
						$resultadoDAO = new Resultado_DAO();
						$listadoR = $resultadoDAO->listarResultadosSuelo($idusuario);
						$tabla = "";



						while($row = $resultadoDAO->getArray($listadoR))
						{

								$eliminar = '<a href=\"controlador/eliminarResultadosSuelo.php?id='.$row['idResultado'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este analisis de suelo?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';
								$tabla.='{
											"Finca":"'.$row['finca'].'",
											"Lote":"'.$row['lote'].'",
											"idSuelo":"'.$row['idSuelo'].'",
											"fechaR":"'.$row['fecha'].'",
											"N":"'.$row['nitrogeno'].'",
											"P":"'.$row['fosforo'].'",
											"K":"'.$row['potasio'].'",
											"Ca":"'.$row['calcio'].'",
											"Zn":"'.$row['zinc'].'",
											"Mg":"'.$row['magnesio'].'",
											"Mn":"'.$row['manganesio'].'",
											"acciones":"'.$eliminar.'"
										},';



	             }



									$tabla = substr($tabla,0, strlen($tabla) - 1);

									return $tabla;



			}






		function listarEmpleadosTabla($idUsuario)
		{

					$finca=new Finca();
					$fincaDAO=new Finca_DAO();
					$finca->setidUsuario($idUsuario);
					$result=$fincaDAO->ListarFincas($finca);



					$tabla = "";
					while ($fincas=$fincaDAO->getArray($result))
					{

						$empleadoDAO=new Empleado_DAO();
						$resultado=$empleadoDAO->ListarEmpleados($fincas['idFinca']);


					while($row=$empleadoDAO->getArray($resultado))
					{
            if($fincas['idFinca']==$row['idFinca'])
						{
						$editar = '<a  data-id=\"'.$row['idEmpleado'].'\"\"type=\"button\" onclick=\"ListarEmpleados('.$row['idEmpleado'].')\" data-toggle=\"modal\" data-target=\"#modalEditE\" data-placement=\"top\" title=\"Editar\" class=\"editar btn btn-primary\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>';
						$eliminar = '<a href=\"controlador/eliminarEmpleado.php?id='.$row['idEmpleado'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este empleado?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';

						$tabla.='{
									"Finca":"'.$fincas['nombre'].'",
								  "idEmpleado":"'.$row['idEmpleado'].'",
								  "Nombre":"'.$row['nombre'].'",
								  "Cedula":"'.$row['cedula'].'",
									"Telefono":"'.$row['telefono'].'",
									"Rol":"'.$row['rol'].'",
								  "acciones":"'.$editar.$eliminar.'"
								},';
						}
      		}

					}

								$tabla = substr($tabla,0, strlen($tabla) - 1);

								return $tabla;



		}


		function ListarLotesTabla($idUsuario)
		{

					$finca=new Finca();
					$fincaDAO=new Finca_DAO();
					$finca->setidUsuario($idUsuario);
					$result=$fincaDAO->ListarFincas($finca);



					$tabla = "";
					while ($fincas=$fincaDAO->getArray($result))
					{

						$loteDAO=new Lote_DAO();
						$resultado=$loteDAO->ListarLotes($fincas['idFinca']);


					while($row=$loteDAO->getArray($resultado))
					{
						if($fincas['idFinca']==$row['idFinca'])
						{
						$editar = '<a  data-id=\"'.$row['idLote'].'\"\"type=\"button\" onclick=\"ListarLotes('.$row['idLote'].')\" data-toggle=\"modal\" data-target=\"#modalELote\" data-placement=\"top\" title=\"Editar\" class=\"editar btn btn-primary\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>';
						$eliminar = '<a href=\"controlador/eliminarLote.php?id='.$row['idLote'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este Lote?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';

						$tabla.='{
									"finca":"'.$fincas['nombre'].'",
									"idLote":"'.$row['idLote'].'",
									"lote":"'.$row['nombre'].'",
									"medida":"'.$row['medida'].'",
									"ubicacion":"'.$fincas['Ciudad'].'",
									"fecha":"'.$row['fecha'].'",
									"estadofenologico":"'.$row['estadofenologico'].'",
									"acciones":"'.$editar.$eliminar.'"
								},';
						}
					}

					}

								$tabla = substr($tabla,0, strlen($tabla) - 1);

								return $tabla;



		}




		function ListarSemillasTabla($idUsuario)
		{

					$finca=new Finca();
					$fincaDAO=new Finca_DAO();
					$finca->setidUsuario($idUsuario);
					$result=$fincaDAO->ListarFincas($finca);



					$tabla = "";
					while ($fincas=$fincaDAO->getArray($result))
					{

						$semillaDAO=new Semilla_DAO();
						$resultado=$semillaDAO->ListarSemillas($fincas['idFinca']);


					while($row=$semillaDAO->getArray($resultado))
					{
						if($fincas['idFinca']==$row['idFinca'])
						{
						$editar = '<a  data-id=\"'.$row['idSemilla'].'\"\"type=\"button\" onclick=\"ListarSemillas('.$row['idSemilla'].')\" data-toggle=\"modal\" data-target=\"#modalESemilla\" data-placement=\"top\" title=\"Editar\" class=\"editar btn btn-primary\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>';
						$eliminar = '<a href=\"controlador/eliminarSemilla.php?id='.$row['idSemilla'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar esta semilla?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';

						$tabla.='{
									"finca":"'.$fincas['nombre'].'",
									"idSemilla":"'.$row['idSemilla'].'",
									"nombre":"'.$row['nombre'].'",
									"tipo":"'.$row['tipo'].'",
									"EpocaCultivo":"'.$row['epocaC'].'",
									"unidadMedida":"'.$row['unidadMedida'].'",
									"peso":"'.$row['peso'].'",
									"acciones":"'.$editar.$eliminar.'"
								},';
						}
					}

					}

								$tabla = substr($tabla,0, strlen($tabla) - 1);

								return $tabla;



		}





		function ListarOtrosTabla($idUsuario)
		{

					$finca=new Finca();
					$fincaDAO=new Finca_DAO();
					$finca->setidUsuario($idUsuario);
					$result=$fincaDAO->ListarFincas($finca);



					$tabla = "";
					while ($fincas=$fincaDAO->getArray($result))
					{

						$otroDAO=new Otro_DAO();
						$resultado=$otroDAO->ListarOtros($fincas['idFinca']);


					while($row=$otroDAO->getArray($resultado))
					{
						if($fincas['idFinca']==$row['idFinca'])
						{
						$editar = '<a  data-id=\"'.$row['idOtro'].'\"\"type=\"button\" onclick=\"ListarOtros('.$row['idOtro'].')\" data-toggle=\"modal\" data-target=\"#modalEOtro\" data-placement=\"top\" title=\"Editar\" class=\"editar btn btn-primary\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>';
						$eliminar = '<a href=\"controlador/eliminarOtro.php?id='.$row['idOtro'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este insumo?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';

						$tabla.='{
									"finca":"'.$fincas['nombre'].'",
									"idOtro":"'.$row['idOtro'].'",
									"nombre":"'.$row['nombre'].'",
									"unidadMedida":"'.$row['unidadMedida'].'",
									"detalle":"'.$row['detalle'].'",
									"acciones":"'.$editar.$eliminar.'"
								},';
						}
					}

					}

								$tabla = substr($tabla,0, strlen($tabla) - 1);

								return $tabla;



		}





		function ListarHerramientasTabla($idUsuario)
		{

					$finca=new Finca();
					$fincaDAO=new Finca_DAO();
					$finca->setidUsuario($idUsuario);
					$result=$fincaDAO->ListarFincas($finca);



					$tabla = "";
					while ($fincas=$fincaDAO->getArray($result))
					{

						$herramientaDAO=new Herramienta_DAO();
						$resultado=$herramientaDAO->ListarHerramientas($fincas['idFinca']);


					while($row=$herramientaDAO->getArray($resultado))
					{
						if($fincas['idFinca']==$row['idFinca'])
						{
						$editar = '<a  data-id=\"'.$row['idHerramienta'].'\"\"type=\"button\" onclick=\"ListarHerramientas('.$row['idHerramienta'].')\" data-toggle=\"modal\" data-target=\"#modalEHerramienta\" data-placement=\"top\" title=\"Editar\" class=\"editar btn btn-primary\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>';

						$eliminar = '<a href=\"controlador/eliminarHerramienta.php?id='.$row['idHerramienta'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar esta Herramienta?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';

						$tabla.='{
									"finca":"'.$fincas['nombre'].'",
									"idHerramienta":"'.$row['idHerramienta'].'",
									"nombre":"'.$row['nombre'].'",
									"uso":"'.$row['uso'].'",
									"cantidad":"'.$row['cantidad'].'",
									"acciones":"'.$editar.$eliminar.'"
								},';
						}
					}

					}

								$tabla = substr($tabla,0, strlen($tabla) - 1);

								return $tabla;



		}


		function eliminarLote($idLote){
			$Lote = new Lote();
			$LoteDao = new Lote_DAO();
			$Lote->setidLote($idLote);
			$result = $LoteDao->eliminarLote($Lote);
			echo "Lote eliminada";
		}


		function eliminarSemilla($idSemilla){
			$Semilla = new Semilla();
			$SemillaDao = new Semilla_DAO();
			$Semilla->setidSemilla($idSemilla);
			$result = $SemillaDao->eliminarSemilla($Semilla);
			echo "Lote eliminada";
		}



		function eliminarOtro($idOtro){
			$Otro = new Otro();
			$OtroDao = new Otro_DAO();
			$Otro->setidOtro($idOtro);
			$result = $OtroDao->eliminarOtro($Otro);
			echo "Lote eliminada";
		}



		function eliminarHerramienta($idHerramienta){
			$Herramienta = new Herramienta();
			$HerramientaDao = new Herramienta_DAO();
			$Herramienta->setidHerramienta($idHerramienta);
			$result = $HerramientaDao->eliminarHerramienta($Herramienta);
			echo "Herramienta eliminada";
		}


		function eliminarEmpleado($idEmpleado){
			$Empleado = new Empleado();
			$EmpleadoDao = new Empleado_DAO();
			$Empleado->setidEmpleado($idEmpleado);
			$result = $EmpleadoDao->eliminarEmpleado($Empleado);
			echo "Empleado eliminado";
		}



	/**
	 *
	 * @param nombre
	 * @param idFinca
	 */
	function registrarLote($nombre, $medida, $fecha, $idFinca,$estadoF)
	{

		$lote = new Lote();
		$loteDAO = new Lote_DAO();
		$lote->setnombre($nombre);
		$lote->setmedida($medida);
		$lote->setfecha($fecha);
		$lote->setFinca($idFinca);
		$lote->setestadoF($estadoF);
		$result=$loteDAO-> AgregarLote($lote);

		if($result!=true){
				echo 'Error al registrar Lote';
		}
		else {
				echo 'Registro Exitoso';
		}

	}



	function registrarSemilla($idFinca,$nombre,$tipo,$epocaC,$unidadM,$peso)
	{

		$semilla = new Semilla();
		$semillaDAO = new Semilla_DAO();
		$semilla->setidFinca($idFinca);
		$semilla->setnombre($nombre);
		$semilla->settipo($tipo);
		$semilla->setepocaC($epocaC);
		$semilla->setunidadM($unidadM);
		$semilla->setpeso($peso);
		$result=$semillaDAO-> AgregarSemilla($semilla);

		if($result!=true){
				echo 'Error al registrar semilla';
		}
		else {
				echo 'Registro Exitoso';
		}

	}



	function registrarOtro($idFinca,$nombre,$unidadM,$detalle)
	{

		$otro = new Otro();
		$otroDAO = new Otro_DAO();
		$otro->setidFinca($idFinca);
		$otro->setnombre($nombre);
		$otro->setunidadM($unidadM);
		$otro->setdetalle($detalle);
		$result=$otroDAO-> AgregarOtro($otro);

		if($result!=true){
				echo 'Error al registrar otro insumo';
		}
		else {
				echo 'Registro Exitoso';
		}

	}




	function registrarHerramienta($idFinca,$nombre,$uso,$cantidad)
	{

		$herramienta = new Herramienta();
		$herramientaDAO = new Herramienta_DAO();
		$herramienta->setidfinca($idFinca);
		$herramienta->setnombre($nombre);
		$herramienta->setuso($uso);
		$herramienta->setcantidad($cantidad);
		$result=$herramientaDAO-> AgregarHerramienta($herramienta);

		if($result!=true){
				echo 'Error al registrar Herramienta';
		}
		else {
				echo 'Registro Exitoso';
		}

	}

	function editarHerramienta($idFinca,$idH,$nombre,$uso,$cantidad)
	{

		$herramienta = new Herramienta();
		$herramientaDAO = new Herramienta_DAO();
		$herramienta->setidfinca($idFinca);
		$herramienta->setidherramienta($idH);
		$herramienta->setnombre($nombre);
		$herramienta->setuso($uso);
		$herramienta->setcantidad($cantidad);
		$result=$herramientaDAO-> editarHerramienta($herramienta);

		if($result!=true){
				echo 'Error al registrar Herramienta';
		}
		else {
				echo 'Registro Exitoso';
		}

	}



	function editarSemilla($idFinca,$idS,$nombre,$tipo,$epocaC,$unidadM,$peso)
	{

		$semilla = new Semilla();
		$semillaDAO = new Semilla_DAO();
		$semilla->setidFinca($idFinca);
		$semilla->setidSemilla($idS);
		$semilla->settipo($tipo);
		$semilla->setnombre($nombre);
		$semilla->setepocaC($epocaC);
		$semilla->setunidadM($unidadM);
		$semilla->setpeso($peso);
		$result=$semillaDAO-> editarSemilla($semilla);

		if($result!=true){
				echo 'Error al registrar Herramienta';
		}
		else {
				echo 'Registro Exitoso';
		}

	}



	function editarOtro($idFinca,$ido,$nombre,$detalle,$unidadM)
	{

		$otro = new Otro();
		$otroDAO = new Otro_DAO();
		$otro->setidFinca($idFinca);
		$otro->setidOtro($ido);
		$otro->setnombre($nombre);
		$otro->setdetalle($detalle);
		$otro->setunidadM($unidadM);

		$result=$otroDAO->editarOtro($otro);

		if($result!=true){
				echo 'Error al registrar otro insumo';
		}
		else {
				echo 'Registro Exitoso';
		}

	}






	function editarEmpleado($idFinca,$idE,$nombre,$cedula,$telefono,$rol)
	{

		$empleado = new Empleado();
		$empleadoDAO = new Empleado_DAO();
		$empleado->setidfinca($idFinca);
		$empleado->setidempleado($idE);
		$empleado->setnombre($nombre);
		$empleado->setCedula($cedula);
		$empleado->setTelefono($telefono);
		$empleado->setRol($rol);
		$result=$empleadoDAO-> editarEmpleado($empleado);

		if($result!=true){
				echo 'Error al registrar Herramienta';
		}
		else {
				echo 'Registro Exitoso';
		}

	}







	function editarFertilizante1($nombre, $estado,$clasificacion, $nitrogeno,$fosforo,$potasio,$calcio,$zinc,$manganesio, $medida,$detalle,$magnesio,$idF)
	{

		$fertilizante = new Fertilizante();
		$fertilizanteDAO = new Fertilizante_DAO();
		$fertilizante->setnombre($nombre);
		$fertilizante->setestado($estado);
		$fertilizante->setclasificacion($clasificacion);
		$fertilizante->setnitrogeno($nitrogeno);
		$fertilizante->setfosforo($fosforo);
		$fertilizante->setpotasio($potasio);
		$fertilizante->setcalcio($calcio);
		$fertilizante->setzinc($zinc);
		$fertilizante->setmagnesio($magnesio);
		$fertilizante->setmanganesio($manganesio);
		$fertilizante->setmedida($medida);
		$fertilizante->setdetalle($detalle);
		$fertilizante->setidFertilizante($idF);
		$result=$fertilizanteDAO->editarFertilizante1($fertilizante);


		if($result!=true){
				echo 'Error al registrar Fertilizante';
		}
		else {
				echo 'Actualizacio Exitosa';
		}

	}





	function registrarEmpleado($idFinca, $nombre, $cedula, $telefono, $rol)
	{

		$empleado = new Empleado();
		$empleadoDAO = new Empleado_DAO();
		$empleado->setidFinca($idFinca);
		$empleado->setNombre($nombre);
		$empleado->setCedula($cedula);
		$empleado->setTelefono($telefono);
		$empleado->setRol($rol);
		$result=$empleadoDAO-> AgregarEmpleado($empleado);

		if($result!=true){
				echo 'Error al registrar Finca';
		}
		else {
				echo 'Registro Exitoso';
		}

	}



      /**
      *lista las Fincas registrada para montarla en un select
      *@param idUsuario
      */

		function ListarFincas($idUsuario)

		{

          $cad="";

					$finca=new Finca();
					$fincaDAO=new Finca_DAO();
					$finca->setidUsuario($idUsuario);
					$resultado=$fincaDAO->ListarFincas($finca);
					while($row=$fincaDAO->getArray($resultado))
					   {
								 $cad.="<option value='".$row['idFinca']."'>".$row['nombre']."</option>";

      					}

           return $cad;

		}


		function ListarHerramienta($idHerramienta)

		{

          $cad="";

					$herramienta=new Herramienta();
					$herramientaDAO=new Herramienta_DAO();
					$herramienta->setidHerramienta($idHerramienta);
					$unicaH=$herramientaDAO->ListarHerramienta($herramienta);
					$row=$herramientaDAO->getArray($unicaH);
					$cad.="


	            <div class='form-group'>
												<input type='hidden' class='form-control' id=idH value=".$row['idHerramienta'].">
	                      <p>Nombre:</p>
	                      <input type='text' class='form-control' id='nombreE' value=".$row['nombre'].">
	                       <span class='input-group-btn'></span>
	             </div>

	             <div class='form-group'>
	                       <p>Uso:</p>
	                       <input type='text' class='form-control' id='usoE' value=".$row['uso'].">
	                       <span class='input-group-btn'></span>
	              </div>
	             <div class='form-group'>
	                       <p>Cantidad:</p>
	                       <input type='text' class='form-control' id='cantidadE' value=".$row['cantidad']."  >
	                        <span class='input-group-btn'></span>
	              </div>";


           return $cad;

		}






		function listarSemilla($idSemilla)

		{

					$cad="";

					$semilla=new Semilla();
					$semillaDAO=new Semilla_DAO();
					$semilla->setidSemilla($idSemilla);
					$unicaH=$semillaDAO->ListarSemilla($semilla);
					$row=$semillaDAO->getArray($unicaH);
					$cad.="


							<div class='form-group'>
												<input type='hidden' class='form-control' id=idS value=".$row['idSemilla'].">
												<p>Nombre:</p>
												<input type='text' class='form-control' id='nombreSE' value=".$row['nombre'].">
												 <span class='input-group-btn'></span>
							 </div>

							 <div class='form-group'>
												<p>Tipo:</p>
												<select class='form-control' id='tipoSE'>
													 <option>Criollo</option>
													 <option>Forastero</option>
													 <option>Trinitario</option>

												</select>
							 </div>

							 <div class='form-group'>
												<p>Epoca de cultivo:</p>
												<select class='form-control' id='epocaCSE'>
													 <option>Enero</option>
													 <option>Febrero</option>
													 <option>Marzo</option>
													 <option>Abril</option>
													 <option>Mayo</option>
													 <option>Junio</option>
													 <option>Julio</option>
													 <option>Agosto</option>
													 <option>Septiembre</option>
													 <option>Octubre</option>
													 <option>Noviembre</option>
													 <option>Diciembre</option>
												</select>
							 </div>

							 <div class='form-group'>
                        <p>Unidad de Medida:</p>
                        <select class='form-control' id='unidadMSE'>
                           <option>Kilogramos</option>
                           <option>gramos</option>
                        </select>
               </div>
							 <div class='form-group'>
												 <p>Peso:</p>
												 <input type='text' class='form-control' id='pesoSE' value=".$row['peso']."  >
													<span class='input-group-btn'></span>
								</div>";


					 return $cad;

		}





		function listarOtro($idotro)

		{

					$cad="";

					$otro=new Otro();
					$otroDAO=new Otro_DAO();
					$otro->setidOtro($idotro);
					$unicaH=$otroDAO->ListarOtro($otro);
					$row=$otroDAO->getArray($unicaH);
					$cad.="


							<div class='form-group'>
												<input type='hidden' class='form-control' id=idO value=".$row['idOtro'].">
												<p>Nombre:</p>
												<input type='text' class='form-control' id='nombreOE' value=".$row['nombre'].">
												 <span class='input-group-btn'></span>
							 </div>




							 <div class='form-group'>
												<p>Unidad de Medida:</p>
												<select class='form-control' id='unidadMOE'>
												<option>Gramo</option>
												<option>Kilogramo</option>
												<option>Centimetro Cubico</option>
												<option>Metro Cubico</option>
												<option>CentiLitro</option>
												<option>Litro</option>
												</select>
							 </div>
							 <div class='form-group'>
												 <p>Detalles:</p>
												 <input type='text' class='form-control' id='detalleOE' value=".$row['detalle']."  >
													<span class='input-group-btn'></span>
								</div>";


					 return $cad;

		}



		function ListarLote($idLote)

		{

					$cad="";

					$lote=new Lote();
					$loteDAO=new Lote_DAO();
					$lote->setidLote($idLote);
					$unicaH=$loteDAO->ListarLoteE($lote);
					$row=$loteDAO->getArray($unicaH);
					$cad.="


							<div class='form-group'>
												<input type='hidden' class='form-control' id=idL value=".$row['idLote'].">
												<p>Nombre:</p>
												<input type='text' class='form-control' id='nombreLE' value=".$row['nombre'].">
												 <span class='input-group-btn'></span>
							 </div>

							 <div class='form-group'>
												 <p>Medida:</p>
												 <input type='text' class='form-control' id='medidaLE' value=".$row['medida'].">
												 <span class='input-group-btn'></span>
								</div>

								<div class='form-group'>
                          <p>Fecha:</p>
                          <input type='date'  class='form-control'  id='fechaLE' required='' value=".$row['fecha']." >
                          <span class='input-group-btn'></span>
                 </div>

							 <div class='form-group'>
							 			<p>Fase fenologica:</p>
							 			<select class='form-control' id='estadoFLE'>
											<option>Productiva</option>

							 				</select>
								</div>";


					 return $cad;

		}




		function listarFinca($idFinca)

		{

					$cad="";

					$finca=new Finca();
					$fincaDAO=new Finca_DAO();
					$finca->setidFinca($idFinca);
					$unicaH=$fincaDAO->ListarFinca($finca);
					$row=$fincaDAO->getArray($unicaH);
					$cad.="


							<div class='form-group'>
												<input type='hidden' class='form-control' id=idF value=".$row['idFinca'].">
												<p>Nombre:</p>
												<input type='text' class='form-control' id='nombreFE' value=".$row['nombre'].">
												 <span class='input-group-btn'></span>
							 </div>

							 <div class='form-group'>
												 <p>Departamento:</p>
												 <select class='form-control'  id='departFE'>
														<option>Cesar</option>
														<option>Guajira</option>
														<option>N.te de santander</option>
												 </select>
								</div>

								<div class='form-group'>
                          <p>Ciudad:</p>
													<select class='form-control'  id='ciudadFE'>
														 <option>Armenia</option>
														 <option>Cucuta</option>
														 <option>Valledupar</option>
													</select>
                 </div>";


					 return $cad;

		}





		function listarEmpleado($idEmpleado)

		{

          $cad="";

					$empleado=new Empleado();
					$empleadoDAO=new Empleado_DAO();
					$empleado->setidEmpleado($idEmpleado);
					$unicaH=$empleadoDAO->ListarEmpleado($empleado);
					$row=$empleadoDAO->getArray($unicaH);
					$cad.="


	            <div class='form-group'>
												<input type='hidden' class='form-control' id=idE value=".$row['idEmpleado'].">
	                      <p>Nombre:</p>
	                      <input type='text' class='form-control' id='nombreEm' value=".$row['nombre'].">
	                       <span class='input-group-btn'></span>
	             </div>

	             <div class='form-group'>
	                       <p>Uso:</p>
	                       <input type='text' class='form-control' id='cedulaEm' value=".$row['cedula'].">
	                       <span class='input-group-btn'></span>
	              </div>
	             <div class='form-group'>
	                       <p>Cantidad:</p>
	                       <input type='text' class='form-control' id='telefonoEm' value=".$row['telefono']."  >
	                        <span class='input-group-btn'></span>
	              </div>

								<div class=''>
                         <p>Rol:</p>
                         <select class='form-control' id='rolEm'>
                            <option>Mantenimiento</option>
                            <option>Administrativo</option>
                         </select>
                </div>

								";


           return $cad;

		}






		function listarFertilizante($idF)

		{

          $cad="";

					$fertilizante=new Fertilizante();
					$fertilizanteDAO=new Fertilizante_DAO();
					$fertilizante->setidFertilizante($idF);
					$unicaH=$fertilizanteDAO->ListarFertilizantes($fertilizante);
					$row=$fertilizanteDAO->getArray($unicaH);
					$cad.="


					<div class='form-group'>
										<input type='hidden' class='form-control' id=idF value=".$row['idFertilizante'].">
										<p>Nombre:</p>
										<input type='text' class='form-control' id='nombreE' placeholder='nombre del fertilizante'  value=".$row['nombre'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
									 <p>Estado:</p>
									 <select class='form-control' id='estadoE'  value=".$row['estado'].">
											<option>Liquido</option>
											<option>Solido</option>
									 </select>
					</div>

					 <div class='form-group'>
										<p>Clasificacion:</p>
										<input type='text' class='form-control' id='clasificacionE' placeholder='puntue el fertilizante de 1 a 5' value=".$row['clasificacion'].">
										 <span class='input-group-btn'></span>
					 </div>
					 <div class='form-group'>
										<p>Nitrogeno:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='nitrogenoE' value=".$row['nitrogeno']." >
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>fosforo:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='fosforoE' value=".$row['fosforo'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>potasio:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='potasioE' value=".$row['potasio'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>calcio:</p>
										<input type='text' class='form-control'placeholder='% del nutriente' id='calcioE' value=".$row['calcio'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>zinc:</p>
										<input type='text' class='form-control'placeholder='% del nutriente' id='zincE' value=".$row['zinc'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>magnesio:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='magnesioE' value=".$row['magnesio'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>manganesio:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='manganesioE' value=".$row['manganesio'].">
										 <span class='input-group-btn'></span>
					 </div>




						<div class=''form-group''>
										<p>Medida:</p>
										<select class='form-control' id='medidaE' value=".$row['idMedida'].">
											 <option>1</option>
										</select>
					 </div>

					 <div class=''form-group''>
										<p>Detalle:</p>
										<textarea class='form-control' name='detalle' rows='5' placeholder='Detalle del fertilizante' id='detalleE' >".$row['detalle']."</textarea>
										<span class='input-group-btn'></span>
					 </div>

								";


           return $cad;

		}


/**
      *lista los Lotes registrados en una finca para montarla en un select
      *@param idUsuario
      */

		function ListarLotes($idFinca)
		{
          $cad="";
					$finca=new Finca();
					$LoteDAO=new Lote_DAO();
					$finca->setidFinca($idFinca);
					$resultado=$LoteDAO->ListarLotesxfinca($finca);
					while($row=$LoteDAO->getArray($resultado))
					    {
								 $cad.="<option value='".$row['idLote']."'>".$row['nombre']."</option>";
      					}
          return $cad;


		}


/**
      *Lista los lotes por usuario
      */
      function ListarLotesxUsuario($idUsuario)
		{
          $cad="";
					$finca=new Finca();
					$LoteDAO=new Lote_DAO();
					$finca->setidFinca($idFinca);
					$resultado=$LoteDAO->ListarLotesxUsuario($idUsuario);
					while($row=$LoteDAO->getArray($resultado))
					    {
								 $cad.="<option value='".$row['idLote']."'>".$row['nombre']."</option>";
      					}
          return $cad;


		}
  /** 
      *Lista departamento
      */

      function listardepartamento()
      {
       $cad="";
           $departamento_DAO=new departamento_DAO();
           $resultado=$departamento_DAO->listardepartamento();
           while($row=$departamento_DAO->getArray($resultado))
					    {
								 $cad.="<option value='".$row['id']."'>".$row['Nombre']."</option>";
      					}
          return $cad;
      }

      /**
      *lista los Lotes registrados en una finca para montarla en un select
      *@param idUsuario
      */

		function listarciudad($id)
		{
          $cad="";
					
					$ciudad_DAO=new ciudad_DAO();
					$resultado=$ciudad_DAO->listarciudad($id);
					while($row=$ciudad_DAO->getArray($resultado))
					    {
								 $cad.="<option value='".$row['id']."'>".$row['Nombre']."</option>";
      					}
          return $cad;


		}




		function listarciudadxtiempo($idUsuario)
		{
          $cad="";					
					$finca=new Finca();
					$FincaDAO=new Finca_DAO();
					$finca->setidUsuario($idUsuario);
					$resultado=$FincaDAO->ListarFincas($finca);
					while($row=$FincaDAO->getArray($resultado))
					    {
								 $cad.="<option value='".$row['Ciudad'].",".$row['departamento']."'>".$row['nombre']."</option>";
      					}
          return $cad;

		}






	/**
	 *
	 * @param zinc
	 * @param manganesio
	 * @param calcio
	 * @param magnesio
	 * @param potasio
	 * @param fosforo
	 * @param nitrogeno
	 * @param idLote
	 */

	function ingresarAnalisisdeSuelo($zinc,$manganesio,$calcio, $magnesio, $potasio, $fosforo, $nitrogeno, $idLote)
	{
		$analisis=new Suelo();
		$sueloDAO=new Suelo_DAO();
		$analisis->setnitrogeno($nitrogeno);
		$analisis->setfosforo($fosforo);
		$analisis->setpotasio($potasio);
		$analisis->setmanganesio($manganesio);
		$analisis->setcalcio($calcio);
		$analisis->setmagnesio($manganesio);
		$analisis->setzinc($zinc);
		$analisis->setlote($idLote);
		$result=$sueloDAO->AgregarSuelo($analisis);

        if($result!=true){
            echo 'Error al Ingresar el los valores del suelo';
        }
        else {
            echo 'Registro Exitoso';
        }


	}


function editarcontrasena($contra,$id)
  {
  	    $usuario = new Usuario();
		$UsuarioDAO = new Usuario_DAO();
		$usuario-> setpassword($contra);
		$usuario-> setidUsuario($id);
		$result=$UsuarioDAO->editarcontrasena($usuario);

		if($result!=true){
				echo 'Error al cambiar contraseña';
		}
		else {
				echo 'Se cambio la contraseña con exito';
		}


   }

   private function passwordaleatorio(){
   	$cad='';
   	for ($i = 1; $i <= 4; $i++) {
   		$d=rand(1,30);
    $cad.=$d;
}
   return (int)$cad;
   }

   function pedircontrasena($user){
   	$contrasena=$this->passwordaleatorio();
   	$usuario = new Usuario();
	$UsuarioDAO = new Usuario_DAO();
	$usuario->setCedula($user);
	$result=$UsuarioDAO->buscar($usuario);
    $row=$UsuarioDAO->getArray($result);
    if($row['cedula']==null ||$row['cedula']==''){
        echo 'No se encuentra el usuario';
    }else{
       $this->editarcontrasena($contrasena,$row['id']);

       $to = "'".$row['correo']."'";
       $subject = "contraseña Olvidada!";
       $txt = "La peticion de su contraseña se realizo satisfactoriamente
              su contraseña es:  ".$contrasena."";
        $headers = "From: Administrador@Admin.com" . "\r\n" .
                    "CC: jdgamez0@fertilcacao.com";

                mail($to,$subject,$txt,$headers);
    }
   }

	/**
    *Lista las fincas y los lotes y los monta en un combo recibe como parame id usuario
    *@param idUsuario
    */
   function listarFincaLotePorUsuario($idFinca){
        return $this->ListarLotes($idFinca);
   }

  function resultadoAnalisis($usuario){
  	$ideal=new Ideal();
  	$idealDAO=new Ideal_DAO();
  	$sueloDAO=new Suelo_DAO();
  	$analisis=new Suelo();
  	$operacionAnalisis=new operacionAnalisis();
  	$resultado=$sueloDAO->buscarSuelo($analisis);
	  while($row=$sueloDAO->getArray($resultado))
		{
  	    $analisis->setnitrogeno($row['nitrogeno']);
		$analisis->setfosforo($row["fosforo"]);
		$analisis->setpotasio($row['potasio']);
		$analisis->setmanganesio($row['manganesio']);
		$analisis->setcalcio($row['calcio']);
		$analisis->setmagnesio($row['magnesio']);
		$analisis->setzinc($row['zinc']);
		$analisis->setlote($row['idLote']);
		$analisis->setidSuelo($row['idSuelo']);

  	    }

  	   $resultado=$idealDAO->ListarIdeal();
	  while($row=$idealDAO->getArray($resultado))
		{
  	    $ideal->setnitrogeno($row['nitrogeno']);
		$ideal->setfosforo($row["fosforo"]);
		$ideal->setpotasio($row['potasio']);
		$ideal->setmanganesio($row['manganesio']);
		$ideal->setcalcio($row['calcio']);
		$ideal->setmagnesio($row['magnesio']);
		$ideal->setzinc($row['zinc']);
		break;
  	    }
       $ResultadoDAO=new Resultado_DAO();
  	   $vector=$operacionAnalisis->resultadoAnalisis($analisis,$ideal,$usuario);
  	   $operacionAnalisis->setidSuelo($analisis->getidSuelo());
  	   $result=$ResultadoDAO->AgregarResultado($operacionAnalisis);
  	   $cad=$this->tablasuelo($analisis);
  	   $cad.=$this->tablaideal($ideal);
  	   $cad.=$this->tablaanalisis($operacionAnalisis);
  	   $mayor=$this->validarFertilizante($analisis->getlote(),$operacionAnalisis->getnitrogeno(),$operacionAnalisis->getfosforo(),$operacionAnalisis->getpotasio());
  	   $cad.="<br><br>".$mayor;
  	   echo $cad;
       return $vector;

  }


  function obtenerMedida($idSuelo){
   $loteDAO=new Lote_DAO();
   $resultado=$loteDAO->ListarLote($idSuelo);
   $medida=$loteDAO->getArray($resultado);
   return $medida['medida'];
  }
function validarFertilizante($idSuelo,$N,$P,$K){
	$FertilDAO=new Fertilizante_DAO();
	$resultadoDAO=new Resultado_DAO();
	$result=$resultadoDAO->obtenerUltimo();
	$idResult=$resultadoDAO->getArray($result);
	$idResultado=$idResult['id'];
	$recomendadoDAO=new Fertilizante_recomendadoDAO();
	$resultado=$FertilDAO->ListarFertilizante();
	$cad="";
	$fertilN="";
	$fertilP="";
	$fertilK="";
	$kgN=0;
	$KgP=0;
	$KgK=0;
	$medida=$this->obtenerMedida($idSuelo);
	while($row=$FertilDAO->getArray($resultado))
		{
		   if($N>0){
			if ($row['nitrogeno']!=0) {
				$nitrogeno=$row['nitrogeno']/2;
                $resultadoN=$N/$nitrogeno;
                $area=10000/$medida;
                $multi=$resultadoN*50;
                $kgN=$multi/$area;
                $fertilN=$row['nombre'];
                $recomendadoDAO->AgregarFertilizanteR($idResultado,$row['idFertilizante']);

			}

		}
			if($P>0){
			if ($row['fosforo']!=0) {
				$nitrogeno=$row['fosforo']/2;
                $resultadoN=$N/$nitrogeno;
                $area=10000/$medida;
                $multi=$resultadoN*50;
                $KgP=$multi/$area;
                $fertilP=$row['nombre'];
                $recomendadoDAO->AgregarFertilizanteR($idResultado,$row['idFertilizante']);
			}
		}
         if($K>0){
			if ($row['potasio']!=0) {
				$nitrogeno=$row['potasio']/2;
                $resultadoN=$N/$nitrogeno;
                $area=10000/$medida;
                $multi=$resultadoN*50;
                $KgK=$multi/$area;
                $fertilK=$row['nombre'];
                $recomendadoDAO->AgregarFertilizanteR($idResultado,$row['idFertilizante']);
			}
		}
		}
		$cad.="<div class='container-fluid' style='width: 50%;position:rigth'>
            <div class='table-responsive'>
            <h2 class='text-center all-tittles'>Fertilizantes Recomendados</h2>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row div-table-row-list' style='background-color:#b9a37e; font-weight:bold;'>
                        <div class='div-table-cell' style='width: 15%;' >#</div>
                        <div class='div-table-cell' style='width: 11%;'>Fertilizante</div>
                        <div class='div-table-cell' style='width: 11%;'>Cantidad(KG)</div>

                    </div>
                </div>
            </div>
            <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row'>
                        <div class='div-table-cell' style='width: 15%;' style='background-color:#DFF0D8'>Nitrogeno</div>
                        <div class='div-table-cell' style='width: 11%; style='color:'>".$fertilN."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$kgN."</div>
                    </div>
                </div>
            </div>
             <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row'>
                        <div class='div-table-cell' style='width: 15%;' style='background-color:#DFF0D8'>Potasio</div>
                        <div class='div-table-cell' style='width: 11%; style='color:'>".$fertilK."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$KgK."</div>
                    </div>
                </div>
            </div>
             <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row'>
                        <div class='div-table-cell' style='width: 15%;' style='background-color:#DFF0D8'>Fosforo</div>
                        <div class='div-table-cell' style='width: 11%; style='color:'>".$fertilP."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$KgP."</div>
                    </div>
                </div>
            </div>
               </div>


        ";
	return $cad ;

  }

  function resultadoAnalisisPorId($usuario,$idSuelo){
    $ideal=new Ideal();
  	$idealDAO=new Ideal_DAO();
  	$sueloDAO=new Suelo_DAO();
  	$analisis=new Suelo();
  	$analisis->setidSuelo($idSuelo);
  	$operacionAnalisis=new operacionAnalisis();
  	$resultado=$sueloDAO->ListarSuelo($analisis);
	  while($row=$sueloDAO->getArray($resultado))
		{
  	    $analisis->setnitrogeno($row['nitrogeno']);
		$analisis->setfosforo($row["fosforo"]);
		$analisis->setpotasio($row['potasio']);
		$analisis->setmanganesio($row['manganesio']);
		$analisis->setcalcio($row['calcio']);
		$analisis->setmagnesio($row['magnesio']);
		$analisis->setzinc($row['zinc']);
		$analisis->setlote($row['idLote']);
		$analisis->setidSuelo($row['idSuelo']);

  	    }

  	   $resultado=$idealDAO->ListarIdeal();
	  while($row=$idealDAO->getArray($resultado))
		{
  	    $ideal->setnitrogeno($row['nitrogeno']);
		$ideal->setfosforo($row["fosforo"]);
		$ideal->setpotasio($row['potasio']);
		$ideal->setmanganesio($row['manganesio']);
		$ideal->setcalcio($row['calcio']);
		$ideal->setmagnesio($row['magnesio']);
		$ideal->setzinc($row['zinc']);
		break;
  	    }
       $ResultadoDAO=new Resultado_DAO();
  	   $vector=$operacionAnalisis->resultadoAnalisis($analisis,$ideal,$usuario);
  	   $operacionAnalisis->setidSuelo($analisis->getidSuelo());
  	   $result=$ResultadoDAO->AgregarResultado($operacionAnalisis);
  	   $cad=$this->tablasuelo($analisis);
  	   $cad.=$this->tablaideal($ideal);
  	  $cad.=$this->tablaanalisis($operacionAnalisis);
  	   $mayor=$this->validarFertilizante($analisis->getlote(),$operacionAnalisis->getnitrogeno(),$operacionAnalisis->getfosforo(),$operacionAnalisis->getpotasio());  
  	   $cad.='<br><br>'.$mayor;
  	   echo $cad;
      return $vector;



  }


  private function tablaanalisis(operacionAnalisis $op){
   $cad="";
       $cad.="<div class='container-fluid' >
            <h2 class='text-center all-tittles'>Resultado Analisis Ingresado</h2>
            <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row div-table-row-list' style='background-color:#DFF0D8; font-weight:bold;'>
                        <div class='div-table-cell' style='width: 6%;' >#</div>
                        <div class='div-table-cell' style='width: 11%;'>Nitrogeno(N)</div>
                        <div class='div-table-cell' style='width: 11%;'>Fosforo(P)</div>
                        <div class='div-table-cell' style='width: 11%;'>Potasio(K)</div>
                        <div class='div-table-cell' style='width: 11%;'>Calcio(Ca)</div>
                        <div class='div-table-cell' style='width: 11%;'>Magnesio(Mg)</div>
                        <div class='div-table-cell' style='width: 11%;'>Manganeso(Mn)</div>
                        <div class='div-table-cell' style='width: 11%;'>Zinc(Zn)</div>

                    </div>
                </div>
            </div>
            <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row'>
                        <div class='div-table-cell' style='width: 6%;' style='background-color:#DFF0D8'></div>
                        <div class='div-table-cell' style='width: 10%; style='color:'>".$op->getnitrogeno()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getfosforo()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getpotasio()."</div>
                         <div class='div-table-cell' style='width: 10%;'>".$op->getcalcio()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getmagnesio()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getmanganesio()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getzinc()."</div>
                    </div>
                </div>
            </div>
            <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row'>
                        <div class='div-table-cell' style='width: 6%;' style='background-color:#DFF0D8'></div>";
                        if($op->getnitrogeno()==0){
                       $cad.="<div class='div-table-cell' style='width: 10%; background-color:#7AFF33;'>Ideal!</div>";

                       }else
                            if($op->getnitrogeno()>0){
                            	$cad.="<div class='div-table-cell' style='width: 10%; background-color:#F32E25'>Falta!</div>";
                                 }else
                                      if($op->getnitrogeno()<0){
                                        $cad.="<div class='div-table-cell' style='width: 10%; background-color:#EC9704;'>Excedido!</div>";
                                      }
                        if($op->getfosforo()==0){

                        $cad.="<div class='div-table-cell' style='width: 10%; background-color:#7AFF33;'>Ideal!</div>";
                          }else
                              if($op->getfosforo()>0){
                              	$cad.="<div class='div-table-cell' style='width: 10%; background-color:#F32E25'>Falta!</div>";
                                }else
                                     if($op->getfosforo()<0){
                                        $cad.="<div class='div-table-cell' style='width: 10%; background-color:#EC9704;'>Excedido!</div>";
                                     }
                        if($op->getpotasio()==0){

                       $cad.="<div class='div-table-cell' style='width: 10%; background-color:#7AFF33;'>Ideal!</div>";
                        }else
                              if($op->getpotasio()>0){
                              	$cad.="<div class='div-table-cell' style='width: 10%; background-color:#F32E25'>Falta!</div>";
                                 }else
                                      if($op->getpotasio()<0){
                                      	$cad.="<div class='div-table-cell' style='width: 10%; background-color:#EC9704;'>Excedido!</div>";
                                      }
                        if($op->getcalcio()==0){

                        $cad.="<div class='div-table-cell' style='width: 10%; background-color:#7AFF33;'>Ideal!</div>";
                         }else
                               if($op->getcalcio()>0){
                               	$cad.="<div class='div-table-cell' style='width: 10%; background-color:#F32E25'>Falta!</div>";
                                  }else
                                       if($op->getcalcio()<0){
                                          $cad.="<div class='div-table-cell' style='width: 10%; background-color:#EC9704;'>Excedido!</div>";
                                       }
                         if($op->getmagnesio()==0){

                       $cad.="<div class='div-table-cell' style='width: 10%; background-color:#7AFF33;'>Ideal!</div>";
                        }else
                               if($op->getmagnesio()>0){
                               $cad.="<div class='div-table-cell' style='width: 10%; background-color:#F32E25'>Falta!</div>";
                                   }else
                                        if($op->getmagnesio()<0){
                                          $cad.="<div class='div-table-cell' style='width: 10%; background-color:#EC9704;'>Excedido!</div>";
                                        }
                        if($op->getmanganesio()==0){

                        $cad.="<div class='div-table-cell' style='width: 10%; background-color:#7AFF33;'>Ideal!</div>";
                    }else
                               if($op->getmanganesio()>0){
                               	$cad.="<div class='div-table-cell' style='width: 10%; background-color:#F32E25'>Falta!</div>";

                                  }else
                                       if($op->getmanganesio()<0){
                                       $cad.="<div class='div-table-cell' style='width: 10%; background-color:#EC9704;'>Excedido!</div>";
                                       }
                        if($op->getzinc()==0){

                        $cad.="<div class='div-table-cell' style='width: 10%; background-color:#7AFF33;'>Ideal!</div>";
                         }else
                               if($op->getzinc()>0){
                               	$cad.="<div class='div-table-cell' style='width: 10%; background-color:#F32E25'>Falta!</div>";

                                 }else
                                      if($op->getzinc()<0){
                                       $cad.="<div class='div-table-cell' style='width: 10%; background-color:#EC9704;'>Excedido!</div>";
                                      }
                    $cad.="</div>
                </div>
            </div>

        </div>";

        return $cad;

  }


  function tablaideal(Ideal $op )
  {
    $cad='';

    $cad="<div class='container-fluid' style='width: 60%;' >
            <h2 class='text-center all-tittles'>Tabla Ideal</h2>
            <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row div-table-row-list' style='background-color:#d8d9da; font-weight:bold;'>
                        <div class='div-table-cell' style='width: 6%;' >#</div>
                        <div class='div-table-cell' style='width: 11%;'>Nitrogeno(N)</div>
                        <div class='div-table-cell' style='width: 11%;'>Fosforo(P)</div>
                        <div class='div-table-cell' style='width: 11%;'>Potasio(K)</div>
                        <div class='div-table-cell' style='width: 11%;'>Calcio(Ca)</div>
                        <div class='div-table-cell' style='width: 11%;'>Magnesio(Mg)</div>
                        <div class='div-table-cell' style='width: 11%;'>Manganeso(Mn)</div>
                        <div class='div-table-cell' style='width: 11%;'>Zinc(Zn)</div>

                    </div>
                </div>
            </div>
            <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row'>
                        <div class='div-table-cell' style='width: 6%;' style='background-color:#DFF0D8'></div>
                        <div class='div-table-cell' style='width: 10%; style='color:'>".$op->getnitrogeno()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getfosforo()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getpotasio()."</div>
                         <div class='div-table-cell' style='width: 10%;'>".$op->getcalcio()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getmagnesio()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getmanganesio()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getzinc()."</div>
                    </div>
                </div>
            </div>
           </div>
            ";
            $cad.="<br><br>";
            return $cad;
  }

  function tablasuelo(Suelo $op )
  {
    $cad='';

    $cad="<div class='container-fluid' style='width: 70%;'>
            <h2 class='text-center all-tittles'>Valores Ingresado</h2>
            <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row div-table-row-list' style='background-color:#cdf1bf; font-weight:bold;'>
                        <div class='div-table-cell' style='width: 6%;' >#</div>
                        <div class='div-table-cell' style='width: 11%;'>Nitrogeno(N)</div>
                        <div class='div-table-cell' style='width: 11%;'>Fosforo(P)</div>
                        <div class='div-table-cell' style='width: 11%;'>Potasio(K)</div>
                        <div class='div-table-cell' style='width: 11%;'>Calcio(Ca)</div>
                        <div class='div-table-cell' style='width: 11%;'>Magnesio(Mg)</div>
                        <div class='div-table-cell' style='width: 11%;'>Manganeso(Mn)</div>
                        <div class='div-table-cell' style='width: 11%;'>Zinc(Zn)</div>

                    </div>
                </div>
            </div>
            <div class='table-responsive'>
                <div class='div-table' style='margin:0 !important;'>
                    <div class='div-table-row'>
                        <div class='div-table-cell' style='width: 6%;' style='background-color:#DFF0D8'></div>
                        <div class='div-table-cell' style='width: 10%; style='color:'>".$op->getnitrogeno()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getfosforo()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getpotasio()."</div>
                         <div class='div-table-cell' style='width: 10%;'>".$op->getcalcio()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getmagnesio()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getmanganesio()."</div>
                        <div class='div-table-cell' style='width: 10%;'>".$op->getzinc()."</div>
                    </div>
                </div>
            </div>
           </div>
            ";
            $cad.="<br><br>";
            return $cad;
  }



}



?>
