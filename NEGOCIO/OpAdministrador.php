<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once '../DTO/Usuario.php';
include_once '../DAO/Usuario_DAO.php';
include_once '../DTO/Administrador.php';
include_once '../DAO/Administrador_DAO.php';
include_once '../DTO/Fertilizante.php';
include_once '../DAO/Fertilizante_DAO.php';
include_once '../DAO/Fertilizante_usuarioDAO.php';
include_once '../DTO/Suelo.php';
include_once '../DAO/Suelo_DAO.php';
include_once '../DTO/Resultado.php';
include_once '../DAO/Resultado_DAO.php';


class OpAdministrador
{


	function __construct() {

    }


	/**
	 *
	 * @param ciudad
	 * @param contraseña
	 * @param ocupacion
	 * @param correo
	 * @param telefono
	 * @param Nombre
	 * @param cedula
	 */
	function registrarUsuario($ciudad, $contrasena, $ocupacion, $correo, $telefono, $Nombre, $cedula)
	{
		$Usuario= new Usuario();
		$UsuarioDAO= new Usuario_DAO();
		$Usuario->setNombre($Nombre);
		$Usuario->setciudad($ciudad);
		$Usuario->setcorreo($correo);
		$Usuario->setocupacion($ocupacion);
		$Usuario->settelefono($telefono);
		$Usuario->setcedula($cedula);
		$Usuario->setpassword($contrasena);
		$result=$UsuarioDAO-> AgregarUsuario($Usuario);

        if($result!=true){
            echo 'Error al registrar Cliente';
        }
        else {
            echo 'Registro Exitoso';
        }

	}




function listarUsuarios()
{
	$Usuario= new Usuario();
	$UsuarioDAO= new Usuario_DAO();

	$tabla = "";
	$result=$UsuarioDAO->listarUsuarios();
    while($row1=$UsuarioDAO->getArray($result))
		{
				$finca=new Finca();
				$fincaDAO=new Finca_DAO();
			    $finca->setidUsuario($row1['idusuario']);
				$fincas=$fincaDAO->cantidadFincas($finca);
				$cantFincas = $fincaDAO->getArray($fincas);
				$loteDAO = new Lote_DAO();
				$lotes=$loteDAO->cantidadLotesU($row1['idusuario']);
				$cantLotes = $loteDAO->getArray($lotes);
				$editar = '<a href='.$row1['idusuario'].'\" data-toggle=\"modal\" data-target=\"#modalEFinca\" type=\"submit\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Editar\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>';
				$eliminar = '<a href=\"controlador/eliminarUsuario.php?id='.$row1['idusuario'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este usuario?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';
				$tabla.='{


							"nombre":"'.$row1['nombre'].'",
							"cedula":"'.$row1['cedula'].'",
							"correo":"'.$row1['correo'].'",
							"ocupacion":"'.$row1['ocupacion'].'",
							"telefono":"'.$row1['telefono'].'",
							"ciudad":"'.$row1['ciudad'].'",
							"fincas":"'.$cantFincas['cantidadF'].'",
							"lotes":"'.$cantLotes['cantidadL'].'",
							"acciones":"'.$editar.$eliminar.'"
						},';
			}

						$tabla = substr($tabla,0, strlen($tabla) - 1);

						return $tabla;



}





function eliminarUsuario($idUsuario){
  $Usuario= new Usuario();
  $UsuarioDao = new Usuario_DAO();
  $Usuario->setidUsuario($idUsuario);
  $result = $UsuarioDao->EliminarUsuario($Usuario);
  echo "Usuario eliminado";
}












	function listarAnalisisSuelo($idusuario)
		{
					$sueloDAO = new Suelo_DAO();
					$listadoS = $sueloDAO->listarAnalisisSuelo($idusuario);
					$tabla = "";



					while($row = $sueloDAO->getArray($listadoS))
					{

							$eliminar = '<a href=\"controlador/eliminarAnalisisSuelo.php?id='.$row['idLote'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este analisis de suelo?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';
							$tabla.='{
										"Finca":"'.$row['finca'].'",
										"Lote":"'.$row['lote'].'",
										"Fecha":"'.$row['fecha'].'",
										"N":"'.$row['nitrogeno'].'",
										"P":"'.$row['fosforo'].'",
										"K":"'.$row['potasio'].'",
										"Ca":"'.$row['calcio'].'",
										"Zn":"'.$row['zinc'].'",
										"Mg":"'.$row['magnesio'].'",
										"Mn":"'.$row['manganesio'].'",
										"acciones":"'.$eliminar.'"
									},';



             }



								$tabla = substr($tabla,0, strlen($tabla) - 1);

								return $tabla;



		}


		function listarAnalisisSinRU($idusuario)
			{
						$sueloDAO = new Suelo_DAO();
						$listadoS = $sueloDAO->listarAnalisisSinRU($idusuario);
						$tabla = "";



						while($row = $sueloDAO->getArray($listadoS))
						{

								$eliminar = '<a href=\"controlador/eliminarAnalisisSuelo.php?id='.$row['idSuelo'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este analisis de suelo?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Generar\" class=\"btn btn-success\"><i class=\"fa fa-tree\" aria-hidden=\"true\"></i></a>';
								$tabla.='{
											"Finca":"'.$row['finca'].'",
											"Lote":"'.$row['lote'].'",
											"Fecha":"'.$row['fecha'].'",
											"N":"'.$row['nitrogeno'].'",
											"P":"'.$row['fosforo'].'",
											"K":"'.$row['potasio'].'",
											"Ca":"'.$row['calcio'].'",
											"Zn":"'.$row['zinc'].'",
											"Mg":"'.$row['magnesio'].'",
											"Mn":"'.$row['manganesio'].'",
											"acciones":"'.$eliminar.'"
										},';



							 }



									$tabla = substr($tabla,0, strlen($tabla) - 1);

									return $tabla;



			}








		function listarResultadosSuelo($idusuario)
			{
						$resultadoDAO = new Resultado_DAO();
						$listadoR = $resultadoDAO->listarResultadosSuelo($idusuario);
						$tabla = "";



						while($row = $resultadoDAO->getArray($listadoR))
						{

								$eliminar = '<a href=\"controlador/eliminarResultadosSuelo.php?id='.$row['idResultado'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este analisis de suelo?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';
								$tabla.='{
											"Finca":"'.$row['finca'].'",
											"Lote":"'.$row['lote'].'",
											"idSuelo":"'.$row['idSuelo'].'",
											"fechaR":"'.$row['fecha'].'",
											"N":"'.$row['nitrogeno'].'",
											"P":"'.$row['fosforo'].'",
											"K":"'.$row['potasio'].'",
											"Ca":"'.$row['calcio'].'",
											"Zn":"'.$row['zinc'].'",
											"Mg":"'.$row['magnesio'].'",
											"Mn":"'.$row['manganesio'].'",
											"acciones":"'.$eliminar.'"
										},';



	             }



									$tabla = substr($tabla,0, strlen($tabla) - 1);

									return $tabla;



			}


			function listarAnalisisSueloA(){
             $sueloDAO = new Suelo_DAO();
					$listadoS = $sueloDAO->listarAnalisisSueloA();
					$tabla = "";
					while($row = $sueloDAO->getArray($listadoS))
					{

							$eliminar = '<a href=\"controlador/eliminarAnalisisSuelo.php?id='.$row['idSuelo'].'\" onclick=\"return confirm(\'¿Seguro que desea eliminar este analisis de suelo?\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>';
							$tabla.='{
										"Finca":"'.$row['finca'].'",
										"Lote":"'.$row['lote'].'",
										"Fecha":"'.$row['fecha'].'",
										"N":"'.$row['nitrogeno'].'",
										"P":"'.$row['fosforo'].'",
										"K":"'.$row['potasio'].'",
										"Ca":"'.$row['calcio'].'",
										"Zn":"'.$row['zinc'].'",
										"Mg":"'.$row['magnesio'].'",
										"Mn":"'.$row['manganesio'].'",
										"Usuario":"'.$row['usuario'].'",
										"acciones":"'.$eliminar.'"
									},';



             }

       $tabla = substr($tabla,0, strlen($tabla) - 1);

									return $tabla;



			}



			function listarFertilizantes()
			{

               $cad="";

					$fertilizante=new Fertilizante();
					$fertilizanteDAO=new Fertilizante_DAO();
					$fertilizante->setidFertilizante($idF);
					$unicaH=$fertilizanteDAO->ListarFertilizantes($fertilizante);
					$row=$fertilizanteDAO->getArray($unicaH);
					$cad.="


					<div class='form-group'>
										<input type='hidden' class='form-control' id=idF value=".$row['idFertilizante'].">
										<p>Nombre:</p>
										<input type='text' class='form-control' id='nombreE' placeholder='nombre del fertilizante'  value=".$row['nombre'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
									 <p>Estado:</p>
									 <select class='form-control' id='estadoE'  value=".$row['estado'].">
											<option>Liquido</option>
											<option>Solido</option>
									 </select>
					</div>

					 <div class='form-group'>
										<p>Clasificacion:</p>
										<input type='text' class='form-control' id='clasificacionE' placeholder='puntue el fertilizante de 1 a 5' value=".$row['clasificacion'].">
										 <span class='input-group-btn'></span>
					 </div>
					 <div class='form-group'>
										<p>Nitrogeno:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='nitrogenoE' value=".$row['nitrogeno']." >
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>fosforo:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='fosforoE' value=".$row['fosforo'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>potasio:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='potasioE' value=".$row['potasio'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>calcio:</p>
										<input type='text' class='form-control'placeholder='% del nutriente' id='calcioE' value=".$row['calcio'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>zinc:</p>
										<input type='text' class='form-control'placeholder='% del nutriente' id='zincE' value=".$row['zinc'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>magnesio:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='magnesioE' value=".$row['magnesio'].">
										 <span class='input-group-btn'></span>
					 </div>

					 <div class='form-group'>
										<p>manganesio:</p>
										<input type='text' class='form-control' placeholder='% del nutriente' id='manganesioE' value=".$row['manganesio'].">
										 <span class='input-group-btn'></span>
					 </div>




						<div class=''form-group''>
										<p>Medida:</p>
										<select class='form-control' id='medidaE' value=".$row['idMedida'].">
											 <option>1</option>
										</select>
					 </div>

					 <div class=''form-group''>
										<p>Detalle:</p>
										<textarea class='form-control' name='detalle' rows='5' placeholder='Detalle del fertilizante' id='detalleE' >".$row['detalle']."</textarea>
										<span class='input-group-btn'></span>
					 </div>

								";


           return $cad;

			}


}

?>
