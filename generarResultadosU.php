<?php
        require_once("otros/encabezado.php");


        session_start();
    if(isset($_SESSION["id"])){?>
         <?php
         require_once("otros/navUsuario.php");

        ?>
        <meta charset="UTF-8">
        <title>Generar Resultados</title>
        <!--CSS-->
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.css">
        <!--Javascript-->

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.bootstrap.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/operaciones.js"></script>

        <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
        </script>

        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Consulta de Resultados<small>.</small></h1>
            </div>
        </div>
        <section class="full-reset text-center" style="padding: 40px 0;">
        <div id="result" class="container">
        <div  class="col-md-8 col-md-offset-2">
            <h1>Generar Resultados</h1>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <table id="ejemplogenerarResultadosU" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Finca</th>
                    <th>Lote</th>
                    <th>Fecha</th>
                    <th>N</th>
                    <th>P</th>
                    <th>K</th>
                    <th>Ca</th>
                    <th>Zn</th>
                    <th>Mg</th>
                    <th>Mn</th>
                    <th>acciones</th>

                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                  <th>Finca</th>
                  <th>Lote</th>
                  <th>Fecha</th>
                  <th>N</th>
                  <th>P</th>
                  <th>K</th>
                  <th>Ca</th>
                  <th>Zn</th>
                  <th>Mg</th>
                  <th>Mn</th>
                  <th>acciones</th>

                </tr>
                </tfoot>
            </table>
        </div>
  </div>

    </section>


        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                </div>
                <div class="modal-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore dignissimos qui molestias ipsum officiis unde aliquid consequatur, accusamus delectus asperiores sunt. Quibusdam veniam ipsa accusamus error. Animi mollitia corporis iusto.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                </div>
            </div>
          </div>
        </div>



        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            UFPS
                        </p>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© Desarrollador:2016 Carlos Alfaro</div>
        </footer>





        </body>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
        </html>
        <?php
        }else{
        	header("location: index.php");
        }
        ?>
