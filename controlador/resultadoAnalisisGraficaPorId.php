<?php
include_once '../FACADE/Facade.php';
session_start();
$usuario=$_SESSION['id'];
$idSuelo=$_POST['idSuelo'];

$facade= Facade::getInstance();
$cad="<div id='canvas-container' class='mx-auto' style='width:40%; margin-left: 30%;'>
<canvas id='myChart' width='400' height='350'>fghjk</canvas></div>";
$result=$facade->resultadoAnalisisPorId($usuario,$idSuelo);
$N=$result['N'];
$P=$result['P'];
$K=$result['K'];
$Ca=$result['Ca'];
$Mg=$result['Mg'];
$Mn=$result['Mn'];
$Zn=$result['Zn'];

$cad.="<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {

        labels: ['Nitrogeno', 'Fosforo', 'Potasio', 'Calcio', 'Magnesio','Manganesio','Zinc'],
        datasets: [{
            label: '# of Votes',
            data: [".$N.",".$P.",". $K.",".$Ca.",".$Mg.",".$Mn.",".$Zn."],
            backgroundColor: [
                'rgba(215, 26, 24, 0.9)', 
                'rgba(255, 61, 19, 0.9)',
                'rgba(255, 206, 86, 0.9)',
                'rgba(75, 192, 192, 0.9)',
                'rgba(153, 102, 255, 0.9)',
                'rgba(153, 102, 255, 0.9)',
                'rgba(255, 159, 64, 0.9)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
})</script>";
echo $cad;

?>