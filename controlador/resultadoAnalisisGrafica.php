<?php
include_once '../FACADE/Facade.php';
session_start();
$usuario=$_SESSION['id'];

$facade= Facade::getInstance();
$cad="<div id='canvas-container' class='mx-auto' style='width:40%; margin-left: 30%;'>
<canvas id='myChart' width='400' height='350'>fghjk</canvas></div>";
$result=$facade->resultadoAnalisis($usuario);
$N=$result['N'];
$P=$result['P'];
$K=$result['K'];
$Ca=$result['Ca'];
$Mg=$result['Mg'];
$Mn=$result['Mn'];
$Zn=$result['Zn'];
$CN='rgba(215, 26, 24, 0.9)';
$CP='rgba(215, 26, 24, 0.9)';
$CK='rgba(215, 26, 24, 0.9)';
$CCa='rgba(215, 26, 24, 0.9)';
$CMg='rgba(215, 26, 24, 0.9)';
$CMn='rgba(215, 26, 24, 0.9)';
$CZn='rgba(215, 26, 24, 0.9)';

//NITROGENO
if($N<0){
   $N=$N*-1;
  if($N>100){
     $N=100;
     $CN='rgba(255, 206, 86, 0.9)';
  }
}else if($N!=0){
    $N=$N*-1;
}
//FOSFORO
if($P<0){
   $P=$P*-1;
  if($P>100){
     $P=100;
     $CP='rgba(255, 206, 86, 0.9)';
  }
}else if($P!=0){$P=$P*-1;}
 //POTASIO
  if($K<0){
   $K=$K*-1;
  if($K>100){
     $K=-100;
     $CK='rgba(255, 206, 86, 0.9)';
  }
}else if($K!=0){$K=$K*-1;}

//CALCIO
  if($Ca<0){
   $Ca=$Ca*-1;
  if($Ca>100){
     $Ca=100;
     $CCA='rgba(255, 206, 86, 0.9)';
  }
}else if($Ca!=0){$Ca=$Ca*-1;}

//MAGNESIO
  if($Mg<0){
   $Mg=$Mg*-1;
  if($Mg>100){
     $Mg=100;
     $CMg='rgba(255, 206, 86, 0.9)';
  }
}else if($Mg!=0){$Mg=$Mg*-1;}

//MANGANESIO
  if($Mn<0){
   $Mn=$Mn*-1;
  if($Mn>100){
     $Mn=100;
     $CMn='rgba(255, 206, 86, 0.9)';
  }
}else if($Mn!=0){$Mn=$Mn*-1;}

//ZINC
  if($Zn<0){
   $Zn=$Zn*-1;
  if($Zn>100){
     $Zn=100;
     $CZn='rgba(255, 206, 86, 0.9)';
  }
}else if($Zn!=0){$Zn=$Zn*-1;}



$cad.="<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {

        labels: ['Nitrogeno', 'Fosforo', 'Potasio', 'Calcio', 'Magnesio','Manganesio','Zinc'],
        datasets: [{
            label: '',
            data: [".$N.",".$P.",". $K.",".$Ca.",".$Mg.",".$Mn.",".$Zn."],
            backgroundColor: [
                'rgba(194, 172, 161, 1)', 
                'rgba(255, 61, 19, 0.9)',
                'rgba(255, 206, 86, 0.9)',
                'rgba(75, 192, 192, 0.9)',
                'rgba(153, 102, 255, 0.9)',
                'rgba(153, 102, 255, 0.9)',
                'rgba(255, 159, 64, 0.9)'
            ],
            borderColor: [
                'rgba(194, 172, 161, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
})</script>";
$cad.="<br><button type='button' class='btn' onclick='pruebaDivAPdf()'>Guardar Pdf</button>";
echo $cad;

?>