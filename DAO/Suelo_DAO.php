<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once ('../otros/conexion2.php');
include_once ('../DTO/Suelo.php');
class Suelo_DAO {



	 private $bd;

    function __construct() {
       $this->bd = conexion2::getInstance();
    } 
   

    public function AgregarSuelo(Suelo $Suelo) {
        
         $this->bd->conection();
         $consulta="INSERT INTO `suelo`(`nitrogeno`, `fosforo`, `potasio`, `magnesio`, `calcio`,`manganesio`, `zinc`, `fecha`, `idLote`) VALUES "
                 . "(".$Suelo->getnitrogeno()
                 	.",".$Suelo->getfosforo().",".$Suelo->getpotasio().",".$Suelo->getmagnesio().",".$Suelo->getcalcio().",".$Suelo->getmanganesio().",".$Suelo->getzinc().","."CURRENT_DATE()".",".$Suelo->getlote().")";
       $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }
      public function ListarSuelo(Suelo $suelo) {
        $this->bd->conection();
        $consulta="SELECT `idSuelo`, `nitrogeno`, `fosforo`, `potasio`, `magnesio`, `calcio`,`manganesio`, `zinc`, `fecha`, `idLote` FROM `suelo` WHERE idSuelo=".$suelo->getidSuelo()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }


    public function ListarSuelos() {
        $this->bd->conection();
        $consulta="SELECT `idSuelo`, `nitrogeno`, `fosforo`, `potasio`, `magnesio`, `calcio`,`manganesio`, `zinc`, `fecha`, `idLote` FROM `suelo` LIMIT 1";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

    public function buscarSuelo(Suelo $Suelo)
    {
       $this->bd->conection();
       $consulta="SELECT max(`idSuelo`) as idSuelo FROM suelo LIMIT 1";
       $result=$this->bd->ejecutarConsultaSQL($consulta);
       $idSuelo=$this->getArray($result);
       $consulta2="SELECT `idSuelo`, `nitrogeno`, `fosforo`, `potasio`, `magnesio`, `calcio`, `manganesio`, `zinc`, `fecha`, `idLote` FROM `suelo` WHERE idSuelo=".$idSuelo['idSuelo']."";
        $result2=$this->bd->ejecutarConsultaSQL($consulta2);
       return $result2;
    }
     public function eliminarAnalisisSuelo($suelo) {
        $this->bd->conection();
        $consulta="delete from suelo where idSuelo=".$suelo."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }



public function ListarAnalisisSuelo($idusuario) {

        $this->bd->conection();
        $consulta="select f.nombre as finca,l.nombre as lote,s.* from usuario u inner join finca f on u.idusuario = f.idUsuario inner join lote l on f.idFinca=l.idFinca inner join suelo s on s.idLote=l.idLote where u.idusuario=$idusuario";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }


    public function listarAnalisisSueloA(){
        $this->bd->conection();
        $consulta="select f.nombre as finca,l.nombre as lote,s.*,u.nombre as usuario from usuario u inner join finca f on u.idusuario = f.idUsuario inner join lote l on f.idFinca=l.idFinca inner join suelo s on s.idLote=l.idLote ";
         $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }


public function listarAnalisisSinRU($idusuario) {

            $this->bd->conection();
            $consulta="select f.nombre as finca,l.nombre as lote,s.* from usuario u inner join finca f on u.idusuario = f.idUsuario inner join lote l on f.idFinca=l.idFinca inner join suelo s on s.idLote=l.idLote where not exists(select r.idResultado from resultado r where r.idSuelo = s.idSuelo) and u.idusuario=$idusuario";
            $result=$this->bd->ejecutarConsultaSQL($consulta);
            return $result;
        }

    public function getArray($result){
        return ($this->bd->getArray($result));
    }
   public function getObject($result){
        return ($this->bd->getObject($result));
    }


}