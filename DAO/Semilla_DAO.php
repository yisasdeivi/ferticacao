<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once ('../otros/conexion2.php');
include_once ('../DTO/Semilla.php');
class Semilla_DAO {
    //put your code here
     private $bd;

    function __construct() {
       $this->bd = conexion2::getInstance();
    }


    public function AgregarSemilla(Semilla $Semilla) {
      $idFinca=$Semilla->getidFinca();
      $nombre=$Semilla->getnombre();
      $tipo=$Semilla->gettipo();
      $epocaC=$Semilla->getepocaC();
      $unidadM=$Semilla->getunidadM();
      $peso=$Semilla->getpeso();

         $this->bd->conection();
         $consulta="INSERT INTO `Semilla`( `idFinca`, `nombre`, `tipo`,`epocaC`,`unidadMedida`, `peso`) VALUES  "
                 . "('".$idFinca."','".$nombre."','".$tipo."','".$epocaC."','".$unidadM."','".$peso."')";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

    public function EditarSemilla(Semilla $semilla) {
      $idFinca=$semilla->getidFinca();
  		$idSemilla=$semilla->getidSemilla();
  		$tipo=$semilla->gettipo();
  		$nombre=$semilla->getnombre();
  		$epocaC=$semilla->getepocaC();
  		$unidadMedida=$semilla->getunidadM();
  		$peso=$semilla->getpeso();


         $this->bd->conection();
         $consulta="update Semilla set nombre = '$nombre' ,tipo='$tipo',epocaC='$epocaC',unidadMedida='$unidadMedida',peso='$peso',idFinca=$idFinca where idSemilla = $idSemilla";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

    public function EliminarSemilla(Semilla $Semilla) {

        $this->bd->conection();
        $consulta="DELETE FROM `Semilla` WHERE `idSemilla`=".$Semilla->getIdSemilla()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

     public function ListarSemilla($Semilla) {
          $idSemilla=$Semilla->getidSemilla();
          $this->bd->conection();
        $consulta="SELECT `idSemilla`, `nombre`, `tipo`, `idFinca` , `epocaC`, `unidadMedida`,`peso` FROM `Semilla` WHERE `idSemilla` = ".$idSemilla."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

    public function ListarSemillas($idFinca) {

          $this->bd->conection();
        $consulta="SELECT `idFinca`, `idSemilla`, `nombre`, `tipo`,  `epocaC`, `unidadMedida`, `peso` FROM `Semilla` WHERE `idFinca` = ".$idFinca."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }
    public function ListarSemillasxfinca(Finca $finca) {

          $this->bd->conection();
        $consulta="SELECT `idSemilla`, `nombre`, `medida`, `idFinca`, `fecha`, `estadofenologico` FROM `Semilla` WHERE `idFinca` =".$finca->getidFinca()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

   public function ListarSemillasxusuario($idUsuario)
   {


    }
    public function cantidadSemillasU($idUsuario) {

          $this->bd->conection();
          $consulta="select count(idSemilla) as cantidadL from Semilla L inner join finca F on L.idFinca=F.idFinca inner join usuario u  on F.idUsuario=$idUsuario group by u.idUsuario";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }



    public function SemillasxFinca($idFinca) {

        $this->bd->conection();
        $consulta="select count(idSemilla) as Semillas from Semilla where idFinca = '$idFinca' group by idFinca";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

    public function getArray($result){
        return ($this->bd->getArray($result));
    }
   public function getObject($result){
        return ($this->bd->getObject($result));
    }


}
