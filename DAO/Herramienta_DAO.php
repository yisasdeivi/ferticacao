<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once ('../otros/conexion2.php');
include_once ('../DTO/Herramienta.php');
class Herramienta_DAO {
    //put your code here
     private $bd;

    function __construct() {
       $this->bd = conexion2::getInstance();
    }


    public function AgregarHerramienta(Herramienta $Herramienta) {
      $idFinca=$Herramienta->getidFinca();
      $nombre=$Herramienta->getnombre();
      $uso=$Herramienta->getuso();
      $cantidad=$Herramienta->getcantidad();


         $this->bd->conection();
         $consulta="INSERT INTO `Herramienta`(`idFinca`,`nombre`, `uso`, `cantidad`) VALUES  "
                 . "('".$idFinca."','".$nombre."','".$uso."','".$cantidad."')";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

    public function editarHerramienta(Herramienta $Herramienta) {
    $nombre=$Herramienta->getnombre();
    $uso=$Herramienta->getuso();
    $cantidad=$Herramienta->getcantidad();
    $idHerramienta=$Herramienta->getidHerramienta();
    $idFinca=$Herramienta->getidfinca();



         $this->bd->conection();
         $consulta="update Herramienta set idFinca= $idFinca, nombre = '$nombre' ,uso= '$uso', cantidad= $cantidad where idHerramienta = $idHerramienta";
          $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

    public function EliminarHerramienta(Herramienta $Herramienta) {

        $this->bd->conection();
        $consulta="DELETE FROM `Herramienta` WHERE `idHerramienta`=".$Herramienta->getidherramienta()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }



    public function ListarHerramientas($idFinca) {

          $this->bd->conection();
        $consulta="SELECT `idFinca`,`idHerramienta`, `nombre`, `uso`, `cantidad`  FROM `Herramienta` WHERE `idFinca` = ".$idFinca."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }


    public function ListarHerramienta($Herramienta) {

        $this->bd->conection();
        $idHerramienta=$Herramienta->getidHerramienta();
        $consulta="SELECT idFinca, idHerramienta, nombre, uso, cantidad FROM Herramienta where idHerramienta=".$idHerramienta."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }
    public function ListarHerramientasxfinca(Finca $finca) {

          $this->bd->conection();
        $consulta="SELECT `idHerramienta`, `nombre`, `medida`, `idFinca`, `fecha`, `estadofenologico` FROM `herramienta` WHERE `idFinca` =".$finca->getidFinca()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

   public function ListarHerramientasxusuario($idUsuario)
   {


    }
    public function cantidadHerramientasU($idUsuario) {

          $this->bd->conection();
          $consulta="select count(idHerramienta) as cantidadL from Herramienta L inner join finca F on L.idFinca=F.idFinca inner join usuario u  on F.idUsuario=$idUsuario group by u.idUsuario";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }



    public function HerramientasxFinca($idFinca) {

        $this->bd->conection();
        $consulta="select count(idHerramienta) as Herramientas from Herramienta where idFinca = '$idFinca' group by idFinca";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

    public function getArray($result){
        return ($this->bd->getArray($result));
    }

    public function getColumns($result){
        return ($this->bd->ingres_fetch_array($result));
    }
   public function getObject($result){
        return ($this->bd->getObject($result));
    }


}
