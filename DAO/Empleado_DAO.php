<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once ('../otros/conexion2.php');
include_once ('../DTO/Empleado.php');
class Empleado_DAO {
    //put your code here
     private $bd;

    function __construct() {
       $this->bd = conexion2::getInstance();
    }


    public function AgregarEmpleado(Empleado $empleado) {
      $idFinca=$empleado->getidFinca();
      $nombre=$empleado->getnombre();
      $cedula=$empleado->getcedula();
      $telefono=$empleado->getTelefono();
      $rol=$empleado->getRol();


         $this->bd->conection();
         $consulta="INSERT INTO `empleado`(`idFinca`, `nombre`, `cedula`,`telefono`,`rol`) VALUES  "
                 . "('".$idFinca."','".$nombre."','".$cedula."','".$telefono."','".$rol."')";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

    public function EditarEmpleado(Empleado $Empleado) {
      $nombre=$Empleado->getnombre();
      $cedula=$Empleado->getcedula();
      $telefono=$Empleado->gettelefono();
      $rol=$Empleado->getrol();
      $idEmpleado=$Empleado->getidEmpleado();
      $idFinca=$Empleado->getidfinca();



           $this->bd->conection();
           $consulta="update Empleado set idFinca= $idFinca, nombre = '$nombre' ,cedula= '$cedula', telefono= '$telefono', rol='$rol' where idEmpleado = $idEmpleado";
            $result=$this->bd->ejecutarConsultaSQL($consulta);
          return $result;
    }

    public function EliminarEmpleado(Empleado $empleado) {

        $this->bd->conection();
        $consulta="DELETE FROM `Empleado` WHERE `idEmpleado`=".$empleado->getidEmpleado()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

     public function ListarEmpleado($Empleado) {

       $this->bd->conection();
       $idEmpleado=$Empleado->getidEmpleado();
       $consulta="SELECT idFinca, idEmpleado, nombre, cedula, telefono, rol FROM Empleado where idEmpleado=".$idEmpleado."";
       $result=$this->bd->ejecutarConsultaSQL($consulta);
       return $result;
    }

    public function ListarEmpleados($idFinca) {

          $this->bd->conection();
        $consulta="SELECT `idFinca`, `idEmpleado`, `nombre`, `cedula`, `telefono` , `rol` FROM `Empleado` WHERE `idFinca` = ".$idFinca."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }
    public function ListarEmpleadosxFinca(Finca $finca) {

          $this->bd->conection();
        $consulta="SELECT `idLote`, `nombre`, `medida`, `idFinca`, `fecha`, `estadofenologico` FROM `lote` WHERE `idFinca` =".$finca->getidFinca()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

   public function ListarLotesxusuario($idUsuario)
   {


    }
    public function cantidadEmpleadosU($idUsuario) {

          $this->bd->conection();
          $consulta="select count(idLote) as cantidadL from lote L inner join finca F on L.idFinca=F.idFinca inner join usuario u  on F.idUsuario=$idUsuario group by u.idUsuario";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }



    public function EmpleadosxFinca($idFinca) {

        $this->bd->conection();
        $consulta="select count(idLote) as lotes from lote where idFinca = '$idFinca' group by idFinca";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

    public function getArray($result){
        return ($this->bd->getArray($result));
    }
   public function getObject($result){
        return ($this->bd->getObject($result));
    }


}
