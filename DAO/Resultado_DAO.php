<?php
include_once ('../otros/conexion2.php');
include_once ('../DTO/operacionAnalisis.php');
class Resultado_DAO {



	 private $bd;

    function __construct() {
       $this->bd = conexion2::getInstance();
    } 
   

    public function AgregarResultado(operacionAnalisis $opera) {
        
         $this->bd->conection();
         $consulta="INSERT INTO `resultado`(`nitrogeno`, `fosforo`, `potasio`, `magnesio`, `calcio`,`manganesio`, `zinc`, `fecha`, `idSuelo`) VALUES "
                 . "(".$opera->getnitrogeno()
            	.",".$opera->getfosforo().",".$opera->getpotasio().",".$opera->getmagnesio().",".$opera->getcalcio().",".$opera->getmanganesio().",".$opera->getzinc().","."CURRENT_DATE()".",".$opera->getidSuelo().")";
       $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $consulta;

    }

    public function EliminarResultado(Resultado $resu) {
        $this->bd->conection();
        $consulta="DELETE FROM `resultado` WHERE `idResultado`=".$resu->getidResultado();
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
 
    }
    
    public function listarResultadosSuelo($idusuario) {

        $this->bd->conection();
        $consulta="select f.nombre as finca,l.nombre as lote,s.idSuelo,r.* from usuario u inner join finca f on u.idusuario = f.idUsuario inner join lote l on f.idFinca=l.idFinca inner join suelo s on s.idLote=l.idLote inner join resultado r on r.idSuelo=s.idSuelo where u.idusuario=$idusuario";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }


    public function obtenerUltimo(){
        $this->bd->conection();
        $consulta="SELECT max(`idResultado`) as id FROM `resultado`";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

    public function ListarResultados() {
        $this->bd->conection();
        $consulta="SELECT `idopera`, `nitrogeno`, `fosforo`, `potasio`, `magnesio`, `calcio`,`manganesio`, `zinc`, `fecha`, `idLote` FROM `opera` LIMIT 1";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

    public function getArray($result){
        return ($this->bd->getArray($result));
    }
   public function getObject($result){
        return ($this->bd->getObject($result));
    }


}

?>