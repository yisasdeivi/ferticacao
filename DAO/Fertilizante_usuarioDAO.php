<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once ('../otros/conexion2.php');
include_once ('../DTO/Fertilizante.php');
class Fertilizante_usuarioDAO {
    //put your code here
     private $bd;

    function __construct() {
       $this->bd = conexion2::getInstance();
    }


    public function listarFxU($idUsuario) {
     $this->bd->conection();
     $consulta= "select idFertilizante from fertilizante_usuario where idusuario = $idUsuario";
     $result=$this->bd->ejecutarConsultaSQL($consulta);
    return $result;

    }

    public function obtenerUltimo() {

     $this->bd->conection();
     $consulta= "select max(idFertilizante) as id from fertilizante";
     $result=$this->bd->ejecutarConsultaSQL($consulta);
     return $result;

    }

    public function AgregarFertilizanteU($idUsuario,$idFertilizante) {


     $this->bd->conection();
     $consulta= "insert into fertilizante_usuario (idFertilizante,idusuario,fecha) values ($idFertilizante,$idUsuario,current_date())";
     $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }




    public function eliminarFusuario(Fertilizante $f) {
        $this->bd->conection();
        $fertilizante=$f->getidFertilizante();
        $consulta="delete from fertilizante_usuario where idFertilizante=".$fertilizante."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }



    public function getArray($result){
        return ($this->bd->getArray($result));
    }
   public function getObject($result){
        return ($this->bd->getObject($result));
    }


}
