<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once ('../otros/conexion2.php');
include_once ('../DTO/Usuario.php');
class Usuario_DAO {
    //put your code here
     private $bd;

    function __construct() {
       $this->bd = conexion2::getInstance();
    }


    public function AgregarUsuario(Usuario $user) {
    $Nombre=$user->getNombre();
    $cedula=$user->getcedula();
    $correo=$user->getcorreo();
    $ciudad=$user->getciudad();
    $departamento=$user->getdepartamento();
    $telefono=$user->gettelefono();
    $ocupacion=$user->getocupacion();
    $contrasena=$user->getpassword();
    $hash =$this->crypt_blowfish($contrasena);
 

    $this->bd->conection();
    $consulta="insert into usuario(nombre, cedula,correo,telefono,ocupacion,password,ciudad) values ('$Nombre','$cedula','$correo','$telefono','$ocupacion','$hash','$ciudad')";
    $result=$this->bd->ejecutarConsultaSQL($consulta);
    return $result;

    }

    public function editarcontrasena(Usuario $user){
      $usuario=$user->getidUsuario();
      $contrase=$user->getpassword();
      $contrasena=$this->crypt_blowfish($contrase);
      $consulta="UPDATE usuario set password='$contrasena' where idusuario=$usuario";
      $result=$this->bd->ejecutarConsultaSQL($consulta);
      return $result;
    }


//ALGORITMO HASH Y APLICAMOS blowfish
     private function crypt_blowfish($password)
      {
        $digito = 7;
        $set_salt = './1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $salt = sprintf('$2a$%02d$', $digito);
        for($i = 0; $i < 22; $i++)
        {
         $salt .= $set_salt[mt_rand(0, 22)];
        }
        return crypt($password, $salt);
      }

      public function Login(Usuario $usuario) {

        $this->bd->conection();
        $consulta="SELECT `idusuario`, `nombre`, `correo`, `cedula`,`password`  FROM `usuario`";
        $result=$this->bd->ejecutarConsultaSQL($consulta);  
        $array=array();
        while($row=$this->getArray($result))
                {
                     $array[]=$row;
                }         
       
        return  $this->validarlogin($array,$usuario);

    }

    private function validarlogin($array,Usuario $user){
     $longitud=count($array);
            foreach($array as $row) 
            { 
               $usuario=$row['cedula']; 
               $dbHash=$row['password'];
               if(($user->getcedula()==$usuario)&&(crypt($user->getpassword(), $dbHash) == $dbHash)){    
                  return $row;                  
               }
            }
            return false;
    }

    public function EliminarUsuario(Usuario  $usuario) {

        $this->bd->conection();
        $consulta="DELETE FROM `usuario` WHERE `idUsuario`=".$usuario->getidUsuario();
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

    public function ListarUsuarios() {

          $this->bd->conection();
        $consulta="SELECT * FROM `usuario`";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

   

    public function infoUsuario(Usuario $Usuario){
      $this->bd->conection();
      $id = $Usuario->getidUsuario();
    $consulta= "SELECT  `nombre`  FROM `usuario` WHERE `idusuario` ='".$id."'";
    $result=$this->bd->ejecutarConsultaSQL($consulta);
    return $result;

    }


    public function buscar(Usuario $Usuario){
      $this->bd->conection();
      $id =$Usuario->getcedula();
    $consulta= "select idusuario as id,nombre,cedula,correo  FROM  usuario  WHERE  cedula =$id";
    $result=$this->bd->ejecutarConsultaSQL($consulta);
    return $result;

    }

    public function getArray($result){
        return ($this->bd->getArray($result));
    }
   public function getObject($result){
        return ($this->bd->getObject($result));
    }


}
