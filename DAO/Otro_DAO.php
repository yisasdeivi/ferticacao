<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once ('../otros/conexion2.php');
include_once ('../DTO/Otro.php');
class Otro_DAO {
    //put your code here
     private $bd;

    function __construct() {
       $this->bd = conexion2::getInstance();
    }


    public function AgregarOtro(Otro $Otro) {
      $idFinca=$Otro->getidFinca();
      $nombre=$Otro->getnombre();
      $unidadM=$Otro->getunidadM();
      $detalle=$Otro->getdetalle();

         $this->bd->conection();
         $consulta="INSERT INTO `otroinsumo`( `idFinca`, `nombre`, `unidadMedida`,`detalle`) VALUES  "
                 . "('".$idFinca."','".$nombre."','".$unidadM."','".$detalle."')";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }


    public function editarOtro(Otro $otro) {
    $nombre=$otro->getnombre();
    $unidadM=$otro->getunidadM();
    $idOtro=$otro->getidOtro();
    $idFinca=$otro->getidFinca();
    $detalle=$otro->getdetalle();

         $this->bd->conection();
         $consulta="update otroinsumo set nombre ='$nombre',unidadMedida='$unidadM',detalle ='$detalle',idFinca=$idFinca where idOtro = $idOtro";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

    public function EliminarOtro(Otro $Otro) {

        $this->bd->conection();
        $consulta="DELETE FROM `otroinsumo` WHERE `idOtro`=".$Otro->getIdOtro()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

     public function ListarOtro($otro) {
          $idOtro=$otro->getidOtro();
          $this->bd->conection();

        $consulta="SELECT `idOtro`, `nombre`, `unidadMedida`, `idFinca` , `detalle` FROM `otroinsumo` WHERE `idOtro` = ".$idOtro."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

    public function ListarOtros($idFinca) {

          $this->bd->conection();
        $consulta="SELECT `idFinca`, `idOtro`, `nombre`, `unidadMedida`,  `detalle` FROM `otroinsumo` WHERE `idFinca` = ".$idFinca."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }
    public function ListarOtrosxfinca(Finca $finca) {

          $this->bd->conection();
        $consulta="SELECT `idOtro`, `nombre`, `medida`, `idFinca`, `fecha`, `estadofenologico` FROM `Otro` WHERE `idFinca` =".$finca->getidFinca()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

   public function ListarOtrosxusuario($idUsuario)
   {


    }
    public function cantidadOtrosU($idUsuario) {

          $this->bd->conection();
          $consulta="select count(idOtro) as cantidadL from Otro L inner join finca F on L.idFinca=F.idFinca inner join usuario u  on F.idUsuario=$idUsuario group by u.idUsuario";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }



    public function OtrosxFinca($idFinca) {

        $this->bd->conection();
        $consulta="select count(idOtro) as Otros from Otro where idFinca = '$idFinca' group by idFinca";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }

    public function getArray($result){
        return ($this->bd->getArray($result));
    }
   public function getObject($result){
        return ($this->bd->getObject($result));
    }


}
