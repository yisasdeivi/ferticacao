<?php

/**
 * @author Gamez
 * @version 1.0
 * @created 05-jun-2017 04:18:38 p.m.
 */

include_once ('../otros/conexion2.php');
include_once ('../DTO/Fertilizante.php');
class Fertilizante_DAO {
    //put your code here
     private $bd;

    function __construct() {
       $this->bd = conexion2::getInstance();
    }


    public function AgregarFertilizante(Fertilizante $fertilizante) {
      $nombre= $fertilizante->getnombre();
  		$estado= $fertilizante->getestado();
  		$clasificacion= $fertilizante->getclasificacion();
  		$nitrogeno= $fertilizante->getnitrogeno();
  		$fosforo= $fertilizante->getfosforo();
  		$potasio= $fertilizante->getpotasio();
  		$calcio= $fertilizante->getcalcio();
  		$zinc= $fertilizante->getzinc();
  		$manganesio= $fertilizante->getmanganesio();
      $magnesio= $fertilizante->getmagnesio();
  		$medida= $fertilizante->getmedida();
  		$detalle= $fertilizante->getdetalle();

     $this->bd->conection();
     $consulta= "insert into fertilizante (nombre, estado,clasificacion, nitrogeno,fosforo,potasio,calcio,zinc,manganesio,magnesio, idMedida, detalle) VALUES ('$nombre', '$estado','$clasificacion', '$nitrogeno','$fosforo','$potasio', '$calcio','$zinc','$manganesio','$magnesio',  '$medida','$detalle')";
     $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }

    public function obtenerUltimo() {

     $this->bd->conection();
     $consulta= "select max(idFertilizante) as id from fertilizante";
     $result=$this->bd->ejecutarConsultaSQL($consulta);
     return $result;

    }



    public function eliminarFertilizantes(Fertilizante $f) {
        $this->bd->conection();
        $consulta="delete from fertilizante where idFertilizante=".$f->getidFertilizante()."";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }




    public function EditarFertilizante1(Fertilizante $fertilizante) {
      $nombre=$fertilizante->getnombre();
  		$estado=$fertilizante->getestado();
  		$clasificacion=$fertilizante->getclasificacion();
  		$nitrogeno=$fertilizante->getnitrogeno();
  		$fosforo=$fertilizante->getfosforo();
  		$potasio=$fertilizante->getpotasio();
  		$calcio=$fertilizante->getcalcio();
  		$zinc=$fertilizante->getzinc();
  		$magnesio=$fertilizante->getmagnesio();
  		$manganesio=$fertilizante->getmanganesio();
  		$medida=$fertilizante->getmedida();
  		$detalle=$fertilizante->getdetalle();
  		$idF=$fertilizante->getidFertilizante();





           $this->bd->conection();
           $consulta="update Fertilizante set nombre= '$nombre', estado = '$estado' ,clasificacion= '$clasificacion', nitrogeno= '$nitrogeno', fosforo='$fosforo', potasio= '$potasio', calcio= '$calcio',  zinc= '$zinc',  magnesio= '$magnesio' , manganesio= '$manganesio',  idMedida= '$medida', detalle= '$detalle' where idFertilizante = $idF";
            $result=$this->bd->ejecutarConsultaSQL($consulta);
          return $result;
    }






    public function EditarFinca(Finca $finca) {
    $Nombre=$finca->getNombre();
    $idUsuario=$finca->getidusuario();
    $idFinca=$finca->getidfinca();



         $this->bd->conection();
         $consulta="INSERT INTO `finca`(`nombre`, `idUsuario`, `Ciudad`,`departamento`) VALUES  "
                 . "('".$Nombre."','".$idUsuario."','".$ciudad."','".$departamento."')";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;

    }


    public function ListarFertilizante(){
        $this->bd->conection();
        $consulta="select f.* from fertilizante f where not exists(select fu.* from fertilizante_usuario fu where fu.idFertilizante = f.idFertilizante);";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
         return $result;
    }

    public function ListarFertilizanteA(){
        $this->bd->conection();
        $consulta="select * from fertilizante";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
         return $result;
    }


  


    public function ListarFertilizantes(Fertilizante $f) {
        $idFertilizante =$f->getidFertilizante();
        $this->bd->conection();
        $consulta="select  * from fertilizante where idFertilizante=$idFertilizante";
        $result=$this->bd->ejecutarConsultaSQL($consulta);
        return $result;
    }



    public function getArray($result){
        return ($this->bd->getArray($result));
    }
   public function getObject($result){
        return ($this->bd->getObject($result));
    }


}
